// TODO: Include your class to test herer
#include "./include/opendocuments.h"
#include "./include/language.h"

#define BOOST_TEST_MODULE MyTest

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <fstream>
#include <memory>

std::string file       = "/home/aron/Corre/gramar1.cpp";
std::string file1      = "/home/aron/Corre/gramar1.cpp";
std::string file2      = "/home/aron/Corre/gramar1.cpp";
std::string fileError  = "/home/aron/Corre/error.cpp";
std::string str        = "#include \"gramar1.h\"\n\ngramar1::gramar1()\n{\n}\n";
std::string str1       = "#include \"gramar1.h\"\n\ngramar1::gramar1()\n{1\n}\n";
std::string str2       = "#include \"gramar1.h\"\n\ngramar1::gramar1()\n{2\n}\n";
std::string strErr     = "int index;\nint main()\n{i°   \n}\n";


BOOST_AUTO_TEST_CASE(project_doc_save_test)
{
        // To simplify this example test, let's suppose we'll test 'float'.
        // Some test are stupid, but all should pass.

        int id = 1;
        int *pr_id = (int*)0x05;

	std::ofstream out(file);
	out << str;
	out.close();

        pBear::corre::openDocuments opr;
        pBear::corre::Project pr = opr.newProject("");

	pBear::corre::Document doc = pr.newDocument(file);
	
	doc.read();


	BOOST_CHECK_EQUAL(doc.getText(), str);
	BOOST_CHECK_EQUAL(doc.getName(), 
		"/home/aron/Corre/gramar1.cpp");
	
	BOOST_CHECK(doc.getSaved() == true);
	doc.setText(doc.getText() + "ii");
	BOOST_CHECK_EQUAL(doc.getText(), str + "ii");
	BOOST_CHECK(doc.getSaved() == false);

	doc.save();
	BOOST_CHECK(doc.getSaved() == true);
	doc.read();
	BOOST_CHECK_EQUAL(doc.getText(),
                std::string("#include \"gramar1.h\"\n\ngramar1::gramar1()\n{\n}\n") + "ii");
	doc.setText("#include \"gramar1.h\"\n\ngramar1::gramar1()\n{\n}\n");
	doc.save();
}

BOOST_AUTO_TEST_CASE(project_doc_multiple_equivalentID_test)
{
	pBear::corre::openDocuments opr;
	pBear::corre::Project pro1 = opr.newProject(file);
	pBear::corre::Project pro2 = opr.newProject(file);
	pBear::corre::Project pro3 = opr.newProject(file);
	pBear::corre::Project pro4 = opr.newProject(file);
	pBear::corre::Project pro5 = opr.newProject(file);

	std::ofstream ou(file);
	std::ofstream ou1(file1);
	std::ofstream ou2(file2);

	ou << str;
	ou1 << str1;
	ou2 << str2;

	BOOST_CHECK_EQUAL(opr.size(), 5);
	pro1.addEquivalentID(1, (int*)0x1);
	pro1.addEquivalentID(2, (int*)0x11);
	pro2.addEquivalentID(1, (int*)0x2);
	pro2.addEquivalentID(2, (int*)0x21);
	pro3.addEquivalentID(1, (int*)0x3);
	pro3.addEquivalentID(2, (int*)0x31);
	pro4.addEquivalentID(1, (int*)0x4);
	pro4.addEquivalentID(2, (int*)0x41);
	pro5.addEquivalentID(1, (int*)0x5);
	pro5.addEquivalentID(2, (int*)0x51);

	BOOST_CHECK_EQUAL(pro1.getEquivalentID(1), (int*)0x1);
	BOOST_CHECK_EQUAL(pro1.getEquivalentID(2), (int*)0x11);
	BOOST_CHECK_EQUAL(pro2.getEquivalentID(1), (int*)0x2);
	BOOST_CHECK_EQUAL(pro2.getEquivalentID(2), (int*)0x21);
	BOOST_CHECK_EQUAL(pro3.getEquivalentID(1), (int*)0x3);
	BOOST_CHECK_EQUAL(pro3.getEquivalentID(2), (int*)0x31);
	BOOST_CHECK_EQUAL(pro4.getEquivalentID(1), (int*)0x4);
	BOOST_CHECK_EQUAL(pro4.getEquivalentID(2), (int*)0x41);
	BOOST_CHECK_EQUAL(pro5.getEquivalentID(1), (int*)0x5);
	BOOST_CHECK_EQUAL(pro5.getEquivalentID(2), (int*)0x51);

	pBear::corre::Document doc11 = pro1.newDocument(file);
	pBear::corre::Document doc12 = pro1.newDocument(file1);
	pBear::corre::Document doc21 = pro2.newDocument(file2);

	BOOST_CHECK_EQUAL(pro1.size(), 2);
	BOOST_CHECK_EQUAL(pro2.size(), 1);
	BOOST_CHECK_EQUAL(pro3.size(), 0);
	BOOST_CHECK_EQUAL(pro4.size(), 0);
	BOOST_CHECK_EQUAL(pro5.size(), 0);

	doc11.addEquivalentID(1, (int*)0x6);
        doc12.addEquivalentID(2, (int*)0x61);
        doc21.addEquivalentID(3, (int*)0x7);

	BOOST_CHECK_EQUAL(doc11.getEquivalentID(1), (int*)0x6);
	BOOST_CHECK_EQUAL(doc12.getEquivalentID(2), (int*)0x61);
	BOOST_CHECK_EQUAL(doc21.getEquivalentID(3), (int*)0x7);
}

int in = 0;

void checkColor(int begin, int end, std::string word)
{
	switch (in)
	{
	case 0:
		BOOST_CHECK(begin == 0 && 3 == end && "#e1aa04" == word);
		break;
	case 1:
		BOOST_CHECK(begin == 3 && 1 == end && word == "#00000");
		break;
	case 2:
		BOOST_CHECK(begin == 4 && 3 == end && word == "#e1aa04");
		break;
	case 3:
		BOOST_CHECK(begin == 7 && 1 == end && word == "#00000");
		break;
	case 4:
		BOOST_CHECK(begin == 8 && 1 == end && word == "#00000");
		break;
	}
	if (in <= 21 and in > 5)
	{
		BOOST_CHECK(word == "#00000");
	}
	in++;
}

std::shared_ptr<pBear::corre::Analizer> ana;
void readyAnalyzer(pBear::corre::Document doc)
{
	using namespace pBear::corre;
	std::vector<DiagnosticData> diags = ana->getDiagnostics();
        BOOST_CHECK(diags.size() == 1);
}

BOOST_AUTO_TEST_CASE(test_language_basic)
{
	std::ofstream ou(fileError);
	ou << strErr;
	ou.close();

	using namespace pBear::corre;
	LanguageDatas *dat = Language::instance()->getLanguage("c++");

	openDocuments ops;

	Project pr = ops.newProject("");

	pBear::corre::Document doc = pr.newDocument(fileError);
	doc.read();
	doc.setText(strErr);
	std::shared_ptr<Colorize> col = dat->getColorize(doc);
	col->setFunctionSetColor(checkColor);

	col->colorizeLine("for(int a = 0; i < 2; i++)");

	ana = dat->getAnalizer(doc);
	ana->setAnalizeData(Analizer::DIAGNOSTIC, 0,0,0);
	ana->run();
	sleep(1);

	std::pair<std::vector<compData>, std::vector<DiagnosticData>> comps = ana->getCompUseful(3, 3, 24);
	auto it = std::find_if(comps.first.begin(), comps.first.end(), [](compData x)
	{
		return x.name == "index";
	});
	BOOST_CHECK(it != comps.first.end());
	BOOST_CHECK(comps.second.size() == 1);
}

BOOST_AUTO_TEST_CASE(test_block_language__basic)
{
	std::ofstream ou(fileError);
        ou << strErr;
        ou.close();

	using namespace pBear::corre;
	openDocuments ops;
	
	Project pr = ops.newProject("");

	pr.setLanguage("c++");

        Document doc = pr.newDocument("/home/aron/Corre/bracket");
	doc.read();

	std::string str = doc.getText().substr(0, 22) + 
		"{\n}\n" + 
		doc.getText().substr(22, doc.getText().size() - 22);
        for (auto it = doc.begin(); it != doc.end(); ++it)
        {
		switch (it->getLine())
		{
		case 0:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 1:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 15);
			break;
		case 2:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 3:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 4:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 14);
			break;
		case 5:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 6:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 7:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 13);
			break;
		case 8:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 12);
			break;
		case 9:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 10:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 11:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 12:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 13:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 14:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 15:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		}
        }

	doc.setText(22, 0, 4, str);


	for (auto it = doc.begin(); it != doc.end(); ++it)
	{
		switch (it->getLine())
		{
		case 0:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 1:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 17);
			break;
		case 2:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 3:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 4);
			break;
		case 4:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 5:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 6:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 16);
			break;
		case 7:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 8:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 9:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 15);
			break;
		case 10:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 14);
			break;
		case 11:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 12:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 13:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 14:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 15:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 16:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 17:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 18:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		}
	}
	str = doc.getText().substr(0, 84) + 
		doc.getText().substr(86, doc.getText().size() - 86);

	doc.setText(84, 2, 0, str);

	for (auto it = doc.begin(); it != doc.end(); ++it)
	{
		switch (it->getLine())
		{
		case 0:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 1:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 17);
			std::cout << it->getEndBlock()->getLine() << std::endl;
			break;
		case 2:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 3:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 4);
			break;
		case 4:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 5:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 6:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 16);
			break;
		case 7:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 8:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 9:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 15);
			break;
		case 10:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 11:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 12:
			BOOST_CHECK(it->isCollapaseble() == true);
			BOOST_CHECK(it->getEndBlock()->getLine() == 13);
			break;
		case 13:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 14:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 15:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 16:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		case 17:
			BOOST_CHECK(it->isCollapaseble() == false);
			break;
		}
	}
	ana.reset();
}

BOOST_AUTO_TEST_CASE(test_analizer__basic)
{
	/*std::ofstream ou(fileError);
        ou << strErr;
        ou.close();

        using namespace pBear::corre;
        openDocuments ops;

        project pr = ops.newProject("");

        pr.setLanguage("c++");

        Document doc = pr.newDocument("/home/aron/Corre/bracket");
        doc.read();

	Analizer *ana = doc.getAnalizer();
	ana->run();
	sleep(1);
	 std::cout << ana->getDiagnostics().size() << std::endl;
	ana->setAnalizeData(Analizer::DIAGNOSTIC, 0,0,0);
	doc.setText(0, 0, 0, "+" + doc.getText());
	std::cout << "ANA: " << ana << std::endl;
	sleep(1);
	std::pair<std::vector<compData>, std::vector<DiagnosticData>> diags = ana->getCompUseful(0, 0, 0);

	std::cout << "....." << std::endl;
	std::cout << "COUTTTT: " << diags.second.size() << std::endl;
	std::cout << ana->getDiagnostics().size() << std::endl;*/
}
