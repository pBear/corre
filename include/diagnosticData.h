#ifndef DIAGNOSTICDATA_H
#define DIAGNOSTICDATA_H

#include <string>
#include <vector>

namespace pBear
{
    namespace corre
    {
        struct fixeData
        {
            std::string         text;
            unsigned            line;
            unsigned            colum;
            unsigned            size;
            bool operator==(const fixeData& fix) const
            {
                return text == fix.text and line == fix.line
                        and colum == fix.colum and size == fix.size;
            }
        };

        enum class gravity   {WARNING,ERROR,FATAL,NOIMPLEMENT, NOTE};

        struct DiagnosticData
        {
            std::string           fileName;
            std::string           text;
            unsigned              line;
            unsigned              minColum;
            unsigned              masSize;
            gravity               grav;
            std::vector<fixeData> fixes;

            bool operator<(const DiagnosticData& data) const
            {
                return line < data.line;
            }
            bool operator>(const DiagnosticData& data) const
            {
                return line > data.line;
            }
            bool operator==(const DiagnosticData& data) const
            {
                return fileName == data.fileName and text == data.text and line == data.line
                        and minColum == data.minColum and masSize == data.masSize
                        and grav == data.grav and fixes == data.fixes;
            }
        };
    }
}

#endif // DIAGNOSTICDATA_H
