#ifndef BLOCK_H
#define BLOCK_H

#include "config.h"
//#include "analizer.h"
#include "diagnosticData.h"

#include <deque>
#include <memory>
#include <list>
#include <map>

namespace pBear
{
    namespace corre
    {
        class RealDocument;
        class Analizer;
        class Document;
        //class DiagnosticData;
        class Block
        {
        public:
            class BlockData {
                Block *bl = nullptr;
            public:
                BlockData(Block *bl): bl(bl) {}
                Block* getBlock() {return bl;}
                void   setBlock(Block *bl) {this->bl = bl;}
            };

            std::shared_ptr<BlockData> data;
            std::map<std::string, void*> pluginData;
        private:
            int                         begin   = 0;
            int                         end     = 0;
            int                         line    = 0;
            void*                       equivalentId;
            bool                        collapsed = false;
            int                         diferenceDistance = 0;
            Block                      *endBlock = nullptr;
            std::weak_ptr<RealDocument> doc;
            std::list<DiagnosticData>   diags;
        public:
            Block(int begin, int end, int line, std::weak_ptr<RealDocument> doc);
            //check
            void update(int begin, int end, int line);
            //check
            int  getBegin() const;
            //check
            int  getEnd()   const;
            //check
            int  getLine()  const;
            //check
            void* getEquivalentId() const;
            //check
            void  setEquivalentId(void* equivalentId);
            //check
            void setCollapsed(bool state);
            //Check
            bool getCollapsed() const;
            //check
            Block* getEndBlock() const;
            //check
            void   setEndBlock(Block *block);
            //check
            int  getDistance() const;
            //check
            void setDiferenceDistance(int value);
            //check
            bool isCollapaseble() const;
            //check
            void updateLanguage();
            //check
            BlockData* getData();
            //check
            std::list<DiagnosticData> &getDiagnostics();
            //check
            void setDiagnostics(std::list<DiagnosticData> diags);
            template<typename type>
            type* getPluginData(std::string name)
            {
                if (pluginData.find(name) == pluginData.end()) return nullptr;
                return static_cast<type*>(pluginData[name]);
            }
            void setPluginData(std::string name, void* pointer)
            {
                pluginData[name] = pointer;
            }
            Document getDocument();
            template<typename type>
            void eleminePluginData(std::string name)
            {
                if (pluginData.find(name) == pluginData.end()) return;
                delete static_cast<type*>(pluginData[name]);
                pluginData.erase(pluginData.find(name));
            }
        };
    }
}

#endif // BLOCK_H
