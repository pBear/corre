/**
 *@author  Aron Surian Wolf
 *@version 1.0
**/

#ifndef DOCUMENT_H
#define DOCUMENT_H

#include "config.h"
#include "block.h"
#include "basedocument.h"
#include "realdocument.h"

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <mutex>
#include <thread>
#include <list>


using std::string;

namespace pBear {
    namespace corre {
        class Project;
        /**
         * @brief The document class is used to store all information refered to a open file. This include text, type...
         */
        class Document: public BaseDocument
        {
        private:
            friend void Block::setCollapsed(bool state);
            std::shared_ptr<RealDocument> ptr;

            RealDocument*                 resolve() const;


        public:
            typedef std::list<Block>::iterator iterator;
            using Type  = RealDocument::Type;
            iterator begin();
            iterator end();
            iterator find(int pos);
        public:
            Document() = default;
            Document(std::shared_ptr<RealDocument> ptr);
            Document(std::string name);
            Document(std::string &name, std::string& text);
            Document(Document const& doc);
            ~Document();
            std::string&                     getText();                                                ///<it return the text of the document
            const std::string&               getName()                         const;                  ///<it return the absolute file path
            bool                             getSaved()                        const;                  ///<it return the saved state;
            const unsigned&                  getVerticalScrollPos()            const;                  ///<it teturn the vertical scroll position
            const unsigned&                  getHorizontalScrollPos()          const;                  ///<it return the horizontal scroll position
            const std::map<int, DOC>        &getEquivalentIDs()                const;                  ///<return the equivalent IDs for the grafic interface
            DOC                              getEquivalentID(int id);                                  ///<return a equivalent interface id for a specific widget
            bool                             getLoadFlag()                     const;                  ///<return the loaded flag
            Project&                         getProyect()                      const;                  ///<return the proyect
            std::string                      getSufix()                        const;                  ///<return the name without the path
            std::string                      getPrefix()                       const;                  ///<return the path without the name
            std::string                      getRelativeNameToPro()            const;                  ///<return a name that are relative to the proyect
            std::shared_ptr<RealDocument>    getPtr()                          const;                  ///<return the original pointer when the document is created
            void                             setScrollPos(unsigned&,unsigned&);                        ///<used to change the scroll pos
            void                             setName(string);                                          ///<used to change the file path
            std::pair<iterator, iterator>    setText(const std::string &);                                          ///<used to change the text
            void                             setLoadFlag(bool flag);                                   ///<change the load flag
            bool                             operator==(Document&)             const;                  ///<used for know if a document is the same like it;
            void                             setSaved(bool _state);                                    ///<set a flag that say that a document is saved
            void                             addEquivalentID(int id, DOC _doc);                        ///<add equivalents a of a equivalent id;
            void                             setProyect(Project _pro);                         ///<set proyect
            void                             read();                                                   ///<read the file
            bool                             istTextEmpty();                                           ///<get if the text is empty or not
            std::vector<std::pair<std::tuple<int,int, int>, std::tuple<int,int,int>>>  getBraquets();  ///<get the braquets in the text
            int                              getFirstOfLine(int)                                const;
            int                              getFirstCharacterOfLine(std::tuple<int,int,int>)   const;
            int                              getLineNumberOfPosition(int index)                 const;
            std::string                      getItem()                                          const;
            void                             setItem(std::string itemName);
            void                             remove();
            void                             save();
            bool                             operator<(const BaseDocument &doc)                     const;
            bool                             operator>(const BaseDocument &doc)  const;
            bool                             operator==(const BaseDocument &doc) const;
            bool                             operator!=(const BaseDocument &doc) const;
            Document&                        operator=(const Document &doc);
            bool                             constainEquivalentId(DOC doc)   const;
            bool                             hasEquivalentId()               const;
            void                             close();
            void                             moveToTrush();
            std::pair<iterator, iterator>         setText(int pos, int charsRemoved, int charsAdded, const std::string& text);
            bool                             empty();
            iterator                         findByLine(int line);
            bool                             renameFile(std::string name);
            void                             updateLanguage();
            Analizer                        *getAnalizer();
            void                             addFunctionAna(std::function<void()> fun, void *pointer);
            void                             quitFunctionAna(void *pointer);
            void                             addFunctionHide(std::function<void(iterator, bool)> fun, void *pointer);
            void                             quitFunctionHide(void *pointer);
            void                             hideSeeBlocks(Block block);
            void                             setType(Type type);
            Type                             getType();
            operator bool() const;
            template<typename type>
            void                             clearBlockData(std::string name)
            {
                for (Block &x: *this)
                {
                    x.eleminePluginData<type>(name);
                }
            }
            std::string                     getExtension();
	    void                            rename(std::string newName);
        };
    }
}

//#define ANALIZER_H

#endif // DOCUMENT_H
