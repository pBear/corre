﻿#ifndef LANGAGEDATA_H
#define LANGAGEDATA_H

//#include "analizer.h"

#include <list>
#include <functional>
#include <string>
#include <vector>
#include <memory>

#include "colorize.h"
#include "analizer.h"
#include "block.h"
#include "updateblockinfo.h"

namespace pBear
{
    class Analizer;
    namespace corre
    {
        class Document;
        class Project;
    }
}

namespace pBear
{
    namespace corre
    {

        class LanguageDatas
        {
        protected:
            std::string              name;
        public:
            struct Item
            {
                std::vector<std::string> extencions;
                std::vector<std::string> pathes;
                std::string              defaultExtencion;
                std::string              name;
                std::string              icon;
                std::string              iconDoc;
                bool                     show;
                bool                     config;
                bool                     showOpenMenu;
                bool                     showAddMenu;
                std::string              defaultEditor;
            };
            struct ComandStructure
            {
                std::string name;
		std::vector<std::pair<std::string, std::string>> files;
                std::vector<std::pair<std::string,std::string>> compile;
                std::vector<std::pair<std::string,std::string>> debug;
                std::vector<std::pair<std::string,std::string>> execute;
                int compileCurrent;
                int debugCurrent;
                int executeCurrent;
		
		std::string path;
		std::string executable;
		
		std::string flags;
		
		std::string getCompileSTR();
		std::string getDebugSTR();
		std::string getExecuteSTR();
            };

        public:
            std::string getName();
            virtual std::shared_ptr<corre::Analizer>        getAnalizer(pBear::corre::Event *event, pBear::corre::Document doc) = 0;
            virtual std::shared_ptr<corre::Colorize>        getColorize(pBear::corre::Document doc) = 0;
            virtual std::vector<Item>                       getItems() = 0;
            virtual std::shared_ptr<Block::BlockData>       getBlockData(Block *bl) = 0;
            virtual std::shared_ptr<corre::UpdateBlockInfo> getUpdateBlockInfo() = 0;
            virtual std::vector<ComandStructure>            getComandStructure() = 0;
            pBear::corre::Document                          addDocument(pBear::corre::Project pro);
        };
    }
}

#endif // LANGAGEDATA_H
