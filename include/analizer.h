#ifndef ANALIZER_H

#define ANALIZER_H
//#include "prodata.h"
#include "pbexeption.h"
#include "document.h"
#include "diagnosticData.h"

#include <string.h>
#include <utility>
#include <algorithm>
#include <set>
#include <mutex>
#include <thread>
#include <atomic>
#include <functional>

using std::vector;
using std::string;
using std::sort;
using std::set;



namespace pBear {
    namespace corre {
        enum class typeCompl {DECLARATION,NORMKEYWORD,STRUCTKEYWORD,FUNCMETHOD};

        struct compData
        {
            std::string              name;
            std::string              coment;
            std::vector<std::string> structure;
            unsigned                 currentParam;
            unsigned                 priority;
        };


        enum class tokenKind {COMENT,LITERAL,KEYWORD,IDENTIFIKER,PUNKTUATION};

        struct tokenData
        {
            tokenKind kind;
            unsigned  colum;
            unsigned  line;
            unsigned  size;
        };

        class Event;

        class Analizer
        {
        protected:
            Document                     doc;
            //diagnsotic
            std::vector<DiagnosticData>  diagnostics;
            //token data
            std::vector<tokenData>       tokens;
            //mutex
            std::recursive_mutex         mu;
            std::mutex                   pause;

            std::unique_ptr<std::thread> th;
            std::once_flag               flag;
            corre::Event                *event;

            std::vector<std::pair<std::function<void()>, void*>> funcs;

        public:
            enum Enable {DIAGNOSTIC = 1, TOKONIZE = 2, ALL = 3};
        protected:
            Enable                  enable;
            unsigned                line   = 0;
            unsigned                colum  = 0;
            unsigned                offset = 0;
            std::atomic<bool>       end;
            std::atomic<bool>       state;
        protected:
            virtual void            updateDiagnostic()         = 0;
            virtual void            updateTokens()             = 0;
            virtual void            analizeText()              = 0;
        private:
            void realRun();
        public:
            void run();
        public:
            //universal
            Analizer(corre::Event *event, Document doc);
            virtual ~Analizer();
            void addFunction(std::function<void()> fun, void *pointer);
            void quitFunction(void *pointer);

            void     setAnalizeData(Enable   _enable,
                                    unsigned _line,
                                    unsigned _colum,
                                    unsigned _offset);
            //thread savelity
            void                          blockThread();
            void                          unlockThread();
            //tokens
            std::vector<tokenData>&       getTokens(); 
            //errors
            std::vector<DiagnosticData>&  getDiagnostics();
            //completions
            virtual std::pair<std::vector<compData>, std::vector<DiagnosticData>> getCompUseful(unsigned line, unsigned column, unsigned offset) = 0;
            virtual std::string getApropiatedInsertText(int pos, std::string str) = 0;
        };
    }
}


#endif // ANALIZER_H
