#ifndef REALPROYECT_H
#define REALPROYECT_H

#include "languageData.h"
//#include "document.h"
#include "config.h"
#include "language.h"
#include "BaseProject.h"
//#include "basedocument.h"
#include "updateblockinfo.h"

#include <string>
#include <set>
#include <memory>
#include <map>
#include <vector>

using std::string;
using std::set;
using std::pair;



namespace pBear
{
    namespace corre
    {
    class Project;
    class openDocuments;
    
    
    /**
     * @brief The proyect class is used to represent a proyect
     */
    class RealProject: public BaseProject
    {
    private:
        std::string                                        name;                        ///<the name of the proyect
        set<Document>                                      docs;                        ///<a data base of documents
        FILEITEMTYPE                                       head   = NULL;               ///<item that represent a head file
        FILEITEMTYPE                                       source = NULL;               ///<item that represent a source file
        proyCompile                                        mode;                        ///<the way that the proyect is compiled
        std::map<std::string, bool>                        headerPathes;                ///<header pathes of the proyect
        std::map<std::string, bool>                        libraryPathes;               ///<library pathes of the proyect
        std::map<int, PROY>                                equivalentIDs;               ///<equivalent interfaces names for the document
        std::string                                        language;
        std::vector<LanguageDatas::Item>                   items;
        std::map<std::string, std::map<int, FILEITEMTYPE>> itemToReal;
        std::string                                        comand;
        std::shared_ptr<UpdateBlockInfo>                   ubi;
        openDocuments*                                     ops = nullptr;
        LanguageDatas::Item                                adtitionalITem;
        std::weak_ptr<RealProject>                         ptr;
        LanguageDatas::ComandStructure                     comandStructure;

        RealProject* resolve();
    public:
        //..............................
        RealProject(string, openDocuments* ops);              ///<constructor used to make a proyect
        void                              setPTR(std::shared_ptr<RealProject> ptr); ///set the shared_ptr to it self(weak_ptr)
        ~RealProject();
        void clear();                                                          ///<destructor necesary to free the doc database (docs)
        iterator                          begin();                           ///<return the begin iterator
        iterator                          end();                             ///<return the end iterator
        string                            getName();                         ///<return the name of the proyect
        void                              setName(string);                   ///<change the name of the proyect
        bool                              elemine(const Document &);                ///<elemine the document using a pointer to the document
        bool                              elemine(iterator);                 ///<elemine the document using a iterator to the document
        iterator                          insert(std::string);                    ///<add a document using name
        iterator                          insert(string, string);            ///<add a document using name and text
        iterator                          insert(Document doc);             ///<add a document using a pointer to this
        iterator                          find(const Document &doc);                   ///<used to find a document in the proyect
        iterator                          find(DOC doc);
        iterator                          find(std::string name);
        std::string&                      getComand();                       ///<get comand
        void                              setCompileMode(proyCompile mode);  ///<set comand mode
        proyCompile                       getCompileMode();                  ///<get comand mode
        void                              setDefaultHeadersPathes();         ///<set default header pathes
        void                              setDefaultLibrarysPathes();        ///<set Default library pathes
        void                              addHeaderPath(std::string  path);  ///<add header path
        void                              addLibraryPath(std::string path);  ///<add Library path
        std::vector<std::string>          getHeaderPathes();                 ///<get header pathes
        std::vector<std::string>          getLibraryPathes();                ///<get library pathes
        void                              addEquivalentID(int id, PROY _pro); ///<add a equivalent id to the proyect
        PROY                              getEquivalentID(int id);            ///<return the equivalent interface ID for a specific implementation identifiqued by id
        std::map<int, PROY>&              getEquivalentIDs();                 ///<return all proyects
        std::string                       getLanguage();
        void                              setLanguage(std::string language);
        const std::vector<LanguageDatas::Item> &getItems();
        LanguageDatas::Item               getItem(std::string item);
        std::string                       getFileType(std::string name);
        std::string                       getRealToItem(FILEITEMTYPE item);
        FILEITEMTYPE                      getItemToReal(std::string item, int id);
        void                              addItem(std::string name, FILEITEMTYPE item, int id);
        std::vector<FILEITEMTYPE>         getRealItemsForId(int id);
        bool                              hasRealItem(FILEITEMTYPE item);
        bool                              hasItem(std::string itemName)                      const;
        bool                              hasItemForId(int id)                               const;
        void                              remove();
        bool                              isEmpty();
        int                               size();
        void                              saveDocs();
        bool                              containDoc(const Document &doc) const;
        bool                              containDoc(DOC doc)             const;
        bool                              containEquivalentId(PROY)       const;
        bool                              operator<(const RealProject& pro)          const;
        bool                              operator>(const RealProject& pro)          const;
        bool                              operator==(const RealProject& pro)         const;
        bool                              operator!=(const RealProject& pro)         const;
        void                              removeRealItemsFor(int id);
        openDocuments*                    getOpenDocuments();
        UpdateBlockInfo*                  getUpdateBlockInfo();
        void                              setComandStructure(LanguageDatas::ComandStructure comandStructure);
        LanguageDatas::ComandStructure*   getComandStructure();
    };
    }
}

#endif // REALPROYECT_H
