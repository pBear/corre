#ifndef PBEXEPTION_H
#define PBEXEPTION_H

#include <exception>
#include <string>
#include <queue>
#include <mutex>
#include <memory>

namespace pBear
{

class pbExeption: public std::exception
{
private:
    std::string             functionName;
    std::string             className;
    std::string             extraInfo;
    unsigned                index         = 0;
    std::queue<std::string> lastFunctions;
public:
    void        setData(std::string, std::string, std::string) noexcept;
    const char* what() const noexcept;
    pbExeption& operator<<(std::string);
    pbExeption& operator<<(void *pointer);
    void        addFunction(std::string func);
    ~pbExeption() {}
};

/**
 * @brief _pbExeption << "function";
    _pbExeption << "class";
    _pbExeption << "exception";
    throw _pbExeption;
 */
extern pbExeption _pbExeption;

}

#endif // PBEXEPTION_H
