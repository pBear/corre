#ifndef COLORIZE_H
#define COLORIZE_H

#include "document.h"

#include <functional>

namespace pBear
{
    namespace corre
    {
        class Colorize
        {
        protected:
            Document doc;
            std::function<void(int, int, std::string)> funSetColor;
        public:
            Colorize(Document doc);
            void setFunctionSetColor(std::function<void(int, int, std::string)> funSetColor);
            virtual bool        colorizeLine(std::string text, bool) = 0;
        };
    }
}

#endif // COLORIZE_H
