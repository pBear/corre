#ifndef EVENT_H
#define EVENT_H

#include "opendocuments.h"
#include "enums.h"
#include "document.h"

#include <vector>
#include <functional>
#include <iostream>
#include <algorithm>


namespace pBear {
    namespace corre {
        class Event
        {
        public:
            //data structs
            struct proData
            {
                PROY         pro    = NULL;
                FILEITEMTYPE header = NULL;
                FILEITEMTYPE source = NULL;
                int          id     = 0;
            };
            struct docData
            {
                DOC data;
                int id;
            };

        private:
            openDocuments ops;

            //typedef
            typedef std::function<void(unsigned)>                              _function_ter_;
            typedef std::vector<std::function<void(unsigned)>>                 _functions_ter_;
            typedef std::function<void(PROY)>                                  _function_pro_;
            typedef std::function<void()>                                      _function_void_;
            typedef std::function<void(Document)>                               _functon_doc_;
            //used for the part that manage the
            typedef std::function<void(Project&)>                              _function_pro_add_;
            typedef std::function<docData(Document&)>                          _function_doc_add_;
            typedef std::function<void(Document,Document::iterator,int,int,int,std::string)> _function_doc_update_;
            typedef std::function<void(Document&)>                             _function_doc_loat_;
            typedef std::function<void(Document&)>                             _function_doc_unload_;
            typedef std::function<void(Document&)>                             _function_doc_close_;
            typedef std::function<void(Project&)>                              _function_laguage_changed_;
            typedef std::function<void(Project)>                               _function_language_changed_Dialog_;
            //part that manage the proyects
            typedef std::function<void(Project)>                               _function_pro_cur_change;
            typedef std::function<void(Project)>                              _function_update_pro_;
            //focus
            typedef std::function<void(identity, Document)> _function_focus_textEdit_change_;
            //tab
            typedef std::function<void(Document,TABID)>                        _function_tab_change_;
            typedef std::function<void(Document, TABID)>                       _function_tab_add_;
            typedef std::function<void(Document, TABID)>                       _function_tab_close_;
            //hide
            typedef std::function<void(unsigned, unsigned, bool)>              _function_hide_blocks_;
            //terminal
            typedef std::function<void(int, std::string)>                      _func_execute_local_term_;
            //structs

            struct dataFunRegularInvoque
            {
                _function_void_                 fun;
                void                           *cl;
                void                           *me;
            };

            //individual functions to run a program



            std::vector<_function_ter_>                             funsTermReady;
            std::vector<_function_ter_>                             funsCompile;
            std::vector<_function_ter_>                             funsExecute;
            //
            std::vector<_function_ter_>                             debuge;
            //
            std::vector<_function_ter_>                             closeTerm;
            //
            std::function<std::string()>                            upCurDoc;
            std::function<void(std::string)>                        upCurDis;
            //
            std::vector<dataFunRegularInvoque>                      funcsRegularInvoque;
            std::vector<_function_void_>                            funcsLongRegularInvoque;
            //
            std::vector<_function_pro_>                             addPro;
            //
            std::vector<_functon_doc_>                              funcsChangeCurDoc;
            ///used for add proyects
            std::vector<std::function<void(Project)>>               funcsAddProy;
            ///used for the part that represent the documents (sub proyects)
            std::vector<std::function<docData(Document)>>           funcsAddDoc;
            ///used to safe the function that change the current proyect in the interface implementation
            std::vector<_function_pro_cur_change>                   funcsProChange;
            ///used to update the proyect data in the ineterface
            std::vector<std::function<void(Project)>>                      funcsProUpdate;
            ///used to update the doc data in the interface;
            std::vector<_function_doc_update_>                      funcsDocUpdate;
            ///used to open a document and loat it
            std::vector<_function_doc_loat_>                        funcsDocLoad;
            ///used to close a document
            std::vector<std::function<void(Document)>>              funcsDocClose;
            ///unload document
            std::vector<std::function<void(Document&)>>             funcsDocUnload;
            ///change textedit focus
            std::vector<_function_focus_textEdit_change_>           funcsFocusEditTextChanged;
            ///change tab
            std::vector<_function_tab_change_>                      funcsTabChanged;
            ///add tab
            std::vector<_function_tab_add_>                         funcsTabAdd;
            std::vector<std::function<void(Project)>>               funcsLanguageChanged;
            _function_language_changed_Dialog_                      functionLanguageChangedDialog;
            std::vector<_function_void_>                            funcsLanguageLoad;
            std::vector<_function_tab_close_>                       funcsTabClose;
            std::vector<_functon_doc_>                              funcsDocMoveToTrush;
            std::vector<_function_hide_blocks_>                     funcsHideBlocks;
            std::vector<_func_execute_local_term_>                  funcsExecuteLocalTerm;
            std::vector<std::function<void(Document)>>              funcsAnalizerReady;
	    std::vector<std::function<void(Document)>>              funcsNormalAddDocument;
	    std::vector<std::function<void(Document)>>              funcsSaveDoc;
	    std::vector<std::function<void(Document)>>              funcsUpdateDoc;

            //used to add function to a funcs
            template<class T1, class T2>
            void addFunction(T1 &fun, T2 &funcs)
            {
                if (fun)
                {
                    funcs.push_back(fun);
                }
                else
                {
                    throw std::runtime_error("no valid function are passed as argument");
                }
            }

            void executeFunc(const _functions_ter_ &funcs, unsigned term);
        public:

            openDocuments* getOpenDocuments();
            //methods
            Event();

            //add
            template<class T>
            void addFuncDebuge(T *_class, void(T::*method)(unsigned))
            {
                addFunction(_class, method, debuge);
                //std::cout << "deb::: " << debuge.size() << std::endl;
            }
            template<class T>
            void addFunCloseTerm(T *_class, void(T::*method)(unsigned))
            {
                addFunction(_class, method, closeTerm);
            }
            template<class T>
            void connectUpdateDoc(T *_class, std::string(T::*method)())
            {
                upCurDoc = std::bind(method, _class);
            }
            template<class T>
            void connectUpdateDis(T *_class, void(T::*method)(std::string))
            {
                upCurDis = std::bind(method, _class, std::placeholders::_1);
            }
            template<class T>
            void addFunAddProyect(T *_class, void(T::*method)(PROY))
            {
                addPro.push_back(std::bind(method, _class, std::placeholders::_1));
            }
            template<class T>
            void addFunRegularInvoque(T *_class, void(T::*method)())
            {
                dataFunRegularInvoque data;
                data.fun = std::bind(method, _class);
                data.cl  = _class;
                #pragma GCC diagnostic ignored "-Wpmf-conversions"
                //data.me  = (void*)method;
                funcsRegularInvoque.push_back(data);
            }
            template<class T>
            void addFunLongRegularInvoque(T *_class, void(T::*method)())
            {
                funcsLongRegularInvoque.push_back(std::bind(method, _class));
            }
            template<class T>
            void addFunChangeCurDoc(T *_class, void(T::*method)(Document))
            {
                funcsChangeCurDoc.push_back(std::bind(method, _class, std::placeholders::_1));
            }
            void addFuncDocChangeCurrent(std::function<void(Document)> fun);
            ///add a function to a list of functions that are executed
            /// when a new Project are added
            void addFunNewProyect(std::function<void(Project)> fun);
            ///add a function to a list of functions that are executed
            /// when a new Document are added
            void addFunNewDocumentGUI(std::function<docData(Document)> fun);
	    void addFunNewDocument(std::function<void(Document)> fun);
            template<class T>
            void addFuncCurrProChange(T *_class, void(T::*method)(Project))
            {
                funcsProChange.push_back(std::bind(method, _class, std::placeholders::_1));
            }
            ///add a function to a list of functions that are executed
            /// when somthing in the project has changed
            void addFuncProUpdate(std::function<void(Project)> fun);
            void addFuncDocUpdate(_function_doc_update_);
            ///add a function to a list of functions that are executed
            /// when a document are loaded
            void addFuncDocLoad(std::function<void(Document)> fun);
            ///add a function to a list of functions that are executed
            /// when a document are closed
            void addFuncDocClose(std::function<void(Document)> fun);
            ///add a function to a list of functions that are executed
            /// when a document are unloaded
            void addFuncDocUnload(std::function<void(Document)> fun);
            template<class T>
            void addFuncFocusTextEditChange(T *_class, void(T::*method)(identity, Document))
            {
                funcsFocusEditTextChanged.push_back(std::bind(method, _class, std::placeholders::_1, std::placeholders::_2));
            }
            template<class T>
            void addFuncChangeTab(T *_class, void(T::*method)(Document,TABID))
            {
                funcsTabChanged.push_back(std::bind(method, _class, std::placeholders::_1, std::placeholders::_2));
            }
            template<class T>
            void addFuncAddTab(T *_class, void(T::*method)(Document,TABID))
            {
                funcsTabAdd.push_back(std::bind(method, _class, std::placeholders::_1, std::placeholders::_2));
            }
            ///add a function to a list of functions that are executed
            /// when language/project type has been changed
            void addFuncChangeLanguage(std::function<void(Project)> fun);
            void setFunctionLanguageChangedDialog(_function_language_changed_Dialog_ fun)
            {
                functionLanguageChangedDialog = fun;
            }
            /*template<class T>
            void addFuncLanguageLoad(T *_class, void(T::*method)())
            {
                funcsLanguageLoad.push_back(std::bind(method, _class));
            }*/
            void addFuncTabClose(_function_tab_close_ fun)
            {
                funcsTabClose.push_back(fun);
            }
            void addFuncDocMoveToTrush(_functon_doc_ fun)
            {
                funcsDocMoveToTrush.push_back(fun);
            }
            void addFunctionHideBlocks(_function_hide_blocks_ fun)
            {
                funcsHideBlocks.push_back(fun);
            }
            void addFuncionExecuteLocalTerminal(_func_execute_local_term_ fun)
            {
                if (funcsExecuteLocalTerm.empty())
                {
                    addFunRegularInvoque(this, &Event::executeFuncLocalTermainalBetThreads);
                }
                funcsExecuteLocalTerm.push_back(fun);

            }
            void addFunctionAnalizerReady(std::function<void(Document)> fun)
            {
                funcsAnalizerReady.push_back(fun);
            }
            
            void addFuncSaveDoc(std::function<void(Document)> fun);
	    void addFuncUpdateDoc(std::function<void(Document)> fun);

            //execute
            void executeFuncDebuge(unsigned term);
            void executeFuncCloseTerm(unsigned term);
            void executeConUpdateDoc();
            void executeConUpdateDis();
            void executeFuncAddProyect(PROY pro);
            void executeFuncRegularInvoque();
            void executeFuncLongRegularInvoque();
            void executeFuncChangeCurDoc(Document _doc);
            //
            void executeFuncNewProyect(Project pro);
            void executeFuncNewDoc(Document &doc);
            void executeFuncChangeCurProy(Project &pro);
            void executeFuncProUpdate(Project pro);
            void executeFuncDocUpdate(pBear::corre::Document doc, corre::Document::iterator it, int pos, int remove, int add,std::string text);
            void executeFuncDocLoad(Document doc);
            void executeFuncDocClose(Document doc);
            void executeFuncDocUnload(Document doc);
            void executeFuncDocMoveToTrush(Document doc);
            //
            void executeFuncEditTextFocusChanged(identity index, Document doc);
            //
            void executeFuncChangeTab(Document doc, TABID tab);
            void executeFuncAddTab(Document doc, TABID tab);
            void executeFuncCloseTab(Document doc, TABID tab);
            //
            void executeFuncLanguageChanged(Project pro);
            void executeFunctionLanguageChangedDialog(Project pro);
            void executeFuncLanguageLoad();
            //
            void executeFuncHideBlocks(unsigned firstLine, unsigned lastLine, bool flag);
            //
            void executeFuncAnalizerReady(Document doc);
	    void executeFuncSaveDoc(Document doc);
	    void executeFuncUpdateDoc(Document doc);

            private:
             int uniqueID;
             std::string comand;
             bool executed = true;
             void executeFuncLocalTermainalBetThreads();
            public:
            void executeFuncLocalTermainal(int uniqueID, std::string comand);
            //quit
            template<class T>
            void quitFuncRegularInvoque(T *_class, void(T::*method)())
            {
                funcsRegularInvoque.erase
                (
                    std::find_if(funcsRegularInvoque.begin(), funcsRegularInvoque.end(), [&](dataFunRegularInvoque data)
                    {
                        return data.cl == (void*)_class && data.me == (void*)method;
                    })
                );
            }
            void quitFunctionHideBlocks()
            {
                funcsHideBlocks.clear();
            }
        };



    }
}

//extern pBear::corre::Event _event;

#endif // EVENT_H
