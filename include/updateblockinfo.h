#ifndef UPDATEBLOCKINFO_H
#define UPDATEBLOCKINFO_H

#include "basedocument.h"
#include "block.h"

#include "document.h"

namespace pBear
{
    namespace corre
    {
        class UpdateBlockInfo
        {
        public:
            virtual void update(std::list<Block>::iterator begin, std::list<Block>::iterator end, corre::Document doc) = 0;
        };
    }
}

#endif // UPDATEBLOCKINFO_H
