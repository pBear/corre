#ifndef REALDOCUMENT_H
#define REALDOCUMENT_H

#include "config.h"
#include "basedocument.h"
#include "block.h"
//#include "languageData.h"

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <mutex>
#include <thread>
#include <list>
#include <atomic>

namespace pBear {
    namespace corre
    {
    class Project;
    class openDocuments;
    class Analizer;
    class Document;
    class UpdateBlockInfo;
    /**
     * @brief The document class is used to store all information refered to a open file. This include text, type...
     */
    class RealDocument: public BaseDocument
    {
    public:
        using iterator = std::list<Block>::iterator;
        enum Type{CONFIG, NORMAL};
    private:
        unsigned                         verticalScroll  = 0;                                            ///<indicate the vertical scroll position. Important when we re-open a file in a window
        unsigned                         horizonalScroll = 0;                                            ///<indicate the horizonal scroll position. Important ehen we re-open a file in a window
        std::string                      text;                                                           ///<used to store the text
        std::string                      name;                                                           ///<the absolute path of the file
        bool                             saved           = true;                                        ///<if it is true the file saved hear is identical to the fisical file
        std::map<int, DOC>               equivalentsIDs;                                                 ///<equivalents IDs used for the interface
        std::unique_ptr<Project>         pro;
        bool                             loaded          = false;                                        ///<the load flack. It indicate if a window is opened
        std::string                      itemName;
        std::list<Block>                 blocks;
        std::shared_ptr<Analizer>        ana;
        std::weak_ptr<RealDocument>      doc;
        Type                             type = NORMAL;

        int                              idInotify;
        std::once_flag                   flag1;
        std::unique_ptr<std::thread>     thr;
        /*struct DocumentLanguage
        {
            std::string         language = "";
            LanguageDatas::Item item;
        } documentLanguage;*/

        std::vector<std::pair<std::function<void()>, void*>> funcsAna;
        std::vector<std::pair<std::function<void(iterator, bool)>, void*>> docHide;
        mutable  std::recursive_mutex   mu;
        std::mutex                      muPrivate;
    public:
        iterator begin();
        iterator end();
        iterator find(int pos);
        void hideSeeBlocks(Block block);
    private:
        void eraseBlocks(iterator begin, int pos, int size);
        iterator getBefore(iterator it);
        void     updateBlock(iterator it);
        int      getNextLineEnd(unsigned pos);
    private:
        void   updateBrackets(iterator _begin, iterator _end);
        int    findBeginOfBracketBlock(int begin,const int& end);
        void checkForErraseBrackets(int pos, int charsRemoved);
        //this methods are linket
        friend void pBear::corre::Block::setCollapsed(bool state);
        void   updateDistanceBracekts(int line, bool mode);
        void   checkFisicalChanges();
    public:
        RealDocument();
        RealDocument(std::string name);
        RealDocument(std::string &name, std::string& text);
        ~RealDocument();
        std::string&                     getText();                                                ///<it return the text of the document
        const std::string&               getName()                         const;                  ///<it return the absolute file path
        bool                             getSaved()                        const;                  ///<it return the saved state;
        const unsigned&                  getVerticalScrollPos()            const;                  ///<it teturn the vertical scroll position
        const unsigned&                  getHorizontalScrollPos()          const;                  ///<it return the horizontal scroll position
        const std::map<int, DOC>        &getEquivalentIDs()                const;                  ///<return the equivalent IDs for the grafic interface
        DOC                              getEquivalentID(int id);                                  ///<return a equivalent interface id for a specific widget
        bool                             getLoadFlag()                     const;                  ///<return the loaded flag
        Project                         &getProyect()                      const;                  ///<return the proyect
        std::string                      getSufix()                        const;                  ///<return the name without the path
        std::string                      getPrefix()                       const;                  ///<return the path without the name
        std::string                      getRelativeNameToPro()            const;                  ///<return a name that are relative to the proyect
        void                             setScrollPos(unsigned&,unsigned&);                        ///<used to change the scroll pos
        void                             setName(std::string);                                          ///<used to change the file path
        std::pair<iterator, iterator>    setText(const std::string &);                                          ///<used to change the text
        void                             setLoadFlag(bool flag);                                   ///<change the load flag
        bool                             operator==(RealDocument&)             const;                  ///<used for know if a document is the same like it;
        void                             setSaved(bool _state);                                    ///<set a flag that say that a document is saved
        void                             addEquivalentID(int id, DOC _doc);                        ///<add equivalents a of a equivalent id;
        void                             setProyect(pBear::corre::Project _pro);                         ///<set proyect
        std::pair<RealDocument::iterator, RealDocument::iterator> read();                                                   ///<read the file
        bool                             istTextEmpty();                                           ///<get if the text is empty or not
        std::vector<std::pair<std::tuple<int,int, int>, std::tuple<int,int,int>>>  getBraquets();  ///<get the braquets in the text
        int                              getFirstOfLine(int)                                const;
        int                              getFirstCharacterOfLine(std::tuple<int,int,int>)   const;
        int                              getLineNumberOfPosition(int index)                 const;
        std::string                      getItem()                                          const;
        void                             setItem(std::string itemName);
        void                             remove();
        void                             save();
        bool                             operator<(const  BaseDocument &doc)                     const;
        bool                             operator>(const  BaseDocument &doc)  const;
        bool                             operator==(const BaseDocument &doc) const;
        bool                             operator!=(const BaseDocument &doc) const;
        bool                             constainEquivalentId(DOC doc)   const;
        bool                             hasEquivalentId()               const;
        void                             close();
        void                             moveToTrush();
        std::pair<iterator, RealDocument::iterator> setText(int pos, int charsRemoved, int charsAdded, const std::string& text);
        bool                             empty();
        iterator                         findByLine(int line);
        bool                             renameFile(std::string name);
        void                             updateLanguage();
        Analizer                        *getAnalizer();
        void                             setAnalizer(std::shared_ptr<Analizer> ana);
        void                             updateDocAnalizer();
        void                             setDoc(std::weak_ptr<RealDocument> doc);
        void                             addFunctionAna(std::function<void()> fun, void *pointer);
        void                             quitFunctionAna(void *pointer);
        void                             addFunctionHide(std::function<void(iterator, bool)> fun, void *pointer);
        void                             quitFunctionHide(void *pointer);
        void                             setType(Type type);
        Type                             getType();
        std::string                      getExtension();
	std::string                      getLanguage();
	std::shared_ptr<UpdateBlockInfo> getUpdateBlockInfo();
	void                             rename(std::string newName);
    };
}
}

#endif // REALDOCUMENT_H
