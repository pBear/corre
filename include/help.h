#ifndef _HELP_HELP_H_
#define _HELP_HELP_H_

#include <string>
#include <array>
#include <vector>
#include <string.h>


std::array<char*, 20> splitStringToArgs(std::string str );
std::vector<std::array<char, 20> > splitStringToSendArrays(std::string name, std::vector<char> opt ,std::string data);

std::array<char, 20> unifySendData(std::string &name, std::vector<char> &opt , std::string data);



std::string extractPart(std::string text,
                        std::string  before,
                        std::string  after,
                        unsigned    *offset = NULL);
std::string extractPart(std::string text,std::string expres);
std::vector<std::string> extractLines(std::string text);
int convertToInt(std::string number);
std::string toStd(int number);
std::string setCorrectSizeType(std::string);
std::string extractPath(std::string fullName);
std::string extractName(std::string fullName);
std::string regexLiteral(std::string name);
std::string getSpecificFileEntryByLine(std::string path, std::string lineName, unsigned entry);
std::string getSpecificFileEntry(std::string path, unsigned entry);

#endif
