#ifndef ENUMS_H
#define ENUMS_H

namespace pBear
{

enum  class split     {ONE, HORIZONTAL, VERTICAL};
enum  class identity  {FIRST, SECOND};

}
#endif // ENUMS_H
