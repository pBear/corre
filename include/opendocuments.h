﻿#ifndef OPENDOCUMENTS_H
#define OPENDOCUMENTS_H

#include "config.h"

#include "document.h"
#include "project.h"

#include <map>
#include <algorithm>
#include <fstream>
#include <vector>
#include <regex>
#include <fstream>
#include <iostream>
#include <dirent.h>
#include <stdio.h>
#include <set>
#include <mutex>
#include <functional>

using std::map;
using std::string;
using std::multimap;
using std::advance;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::vector;
using std::find;
using std::regex_match;
using std::regex;
using std::smatch;
using std::regex_search;
using std::stringstream;
using std::set;
using std::pair;


namespace pBear {
namespace corre {
enum class itemType {NONE,DOCUMENT,PROYECT,GROUP};

class dirEnt {
public:
    enum type {FOLDER, DOCUMENT};
private:
    type        ty;
    std::string name;
    std::string completName;
public:
    //constructor
    dirEnt(std::string _name, std::string _completName, type _ty);
    dirEnt() = default;
    //destructor
    ~dirEnt();
    //get
    type getType();
    std::string getName();
    std::string getCompletName();
    std::string getPrefix();
    //set
    void setName(std::string _name);
    void setType(type _ty);
    void setCompletName(std::string _name);
};

class Event;

class openDocuments
{

private:
    set<pBear::corre::Project> pro;
    std::recursive_mutex       mu1;
    std::weak_ptr<pBear::corre::RealDocument> cDoc;
    std::weak_ptr<pBear::corre::RealProject>      cPro;
    Event                     *event = nullptr;
    std::string                defaultConfigItem;

public:
    using iterator = set<pBear::corre::Project>::iterator;
    openDocuments(){defaultConfigItem="config";}
    ~openDocuments();
    void                       setDefaultConfigItem(std::string item);
    std::string                getDefaultConfigItem();
    iterator                   begin();
    iterator                   end();
    void                       setEvent(Event *event);
    Event*                     getEvent();
    bool                       existProyect(PROY);
    bool                       existProyect(Project &pr);
    bool                       existDocument(DOC doc);
    bool                       existDocument(const Document &doc);
    bool                       existItem(FILEITEMTYPE item);
    Document                   traductDoc(DOC);
    Project                    traductPro(PROY);
    void                       closeProyect(Project);
    void                       setCurrentDoc(Document &);
    Document                   getCurrentDoc() const;
    Project                    getCurrentPro() const;
    void                       setCurrentPro(Project &pro);
    bool                       addProyect(string, PROY);
    void                       addProyect(Project _pro);
    bool                       existFile(string);
    void                       removePro(Project _pro);
    itemType                   type(ITEM);
    vector<string>             redRDir(string name);
    bool                       docEmpty();
    vector<Document>           getUnsavedDocs(Document);
    const vector<Document>     getDocData();
    const map<PROY,Project*>&  getProyData();
    bool                       proEmpty();
    Document                   getDocByName(std::string path);
    bool                       existDocByName(std::string);
    static std::vector<dirEnt> getFolderEntries(std::string path, bool getUnique = false);
    //static bool              compareSearch(std::string file, std::string search);
    Project                    getProByGroup(FILEITEMTYPE _group);
    //bool                     renameDoc(document *oldDoc, document &newDoc);
    //std::string              extractLocationFromPath(std::string path);
    Project                    newProject(std::string name);
    Project                    newProject(std::string name, std::string language, std::string mode);
    int                        size() const;
    template<typename type>
    void                       clearBlockData(std::string name)
    {
        for (Project pro: *this)
        {
            if (!pro) continue;
            pro.clearBlockData<type>(name);
        }
    }
};
}
}

extern pBear::corre::openDocuments *_openDocuments;
#endif
