#ifndef BASEBaseProyect_H
#define BASEBaseProyect_H

#include "languageData.h"
#include "config.h"
#include "language.h"

#include <string>
#include <set>
#include <memory>
#include <map>
#include <vector>

using std::string;
using std::set;
using std::pair;



namespace pBear
{
    namespace corre
    {
    class Document;
    class openDocuments;
    
    class ExceptionNotValidProjectPah: public std::runtime_error 
    {
    public:
      ExceptionNotValidProjectPah(std::string problem):std::runtime_error(problem){};
    };
    
    /**
     * @brief The proyType enum is used to indicate the programer language
     */

    enum class proyCompile
    {
        COMAND,
        CMAKE
    };

    /**
     * @brief The BaseProyect class is used to represent a BaseProyect
     */
    class BaseProject
    {
    public:
        typedef set<Document>::iterator iterator;                          ///<iterator used to acces the docs
        void clear();                                                          ///<destructor necesary to free the doc database (docs)
        iterator                          begin();                           ///<return the begin iterator
        iterator                          end();                             ///<return the end iterator
        string                            getName();                         ///<return the name of the BaseProyect
        void                              setName(string);                   ///<change the name of the BaseProyect
        bool                              elemine(const Document &);                ///<elemine the document using a pointer to the document
        bool                              elemine(iterator);                 ///<elemine the document using a iterator to the document
        iterator                          insert(std::string);                    ///<add a document using name
        iterator                          insert(string, string);            ///<add a document using name and text
        iterator                          insert(const Document &doc);             ///<add a document using a pointer to this
        iterator                          find(const Document &doc);                   ///<used to find a document in the BaseProyect
        iterator                          find(DOC doc);
        iterator                          find(std::string name);
        std::string&                      getComand();                       ///<get comand
        void                              setCompileMode(proyCompile mode);  ///<set comand mode
        proyCompile                       getCompileMode();                  ///<get comand mode
        void                              setDefaultHeadersPathes();         ///<set default header pathes
        void                              setDefaultLibrarysPathes();        ///<set Default library pathes
        void                              addHeaderPath(std::string  path);  ///<add header path
        void                              addLibraryPath(std::string path);  ///<add Library path
        std::vector<std::string>          getHeaderPathes();                 ///<get header pathes
        std::vector<std::string>          getLibraryPathes();                ///<get library pathes
        void                              addEquivalentID(int id, PROY _pro); ///<add a equivalent id to the BaseProyect
        PROY                              getEquivalentID(int id);            ///<return the equivalent interface ID for a specific implementation identifiqued by id
        std::map<int, PROY>&              getEquivalentIDs();                 ///<return all BaseProyects
        std::string                       getLanguage();
        void                              setLanguage(std::string language);
        const std::vector<LanguageDatas::Item> &getItems();
        LanguageDatas::Item               getItem(std::string item);
        std::string                       getFileType(std::string name);
        std::string                       getRealToItem(FILEITEMTYPE item);
        FILEITEMTYPE                      getItemToReal(std::string item, int id);
        void                              addItem(std::string name, FILEITEMTYPE item, int id);
        std::vector<FILEITEMTYPE>         getRealItemsForId(int id);
        bool                              hasRealItem(FILEITEMTYPE item);
        bool                              hasItem(std::string itemName)                      const;
        bool                              hasItemForId(int id)                               const;
        void                              remove();
        bool                              isEmpty();
        int                               size();
        void                              saveDocs();
        bool                              containDoc(const Document &doc) const;
        bool                              containDoc(DOC doc)             const;
        bool                              containEquivalentId(PROY)       const;
        std::shared_ptr<BaseProject>          getPtr();
        bool                              operator<(BaseProject pro)          const;
        bool                              operator>(BaseProject pro)          const;
        bool                              operator==(BaseProject pro)         const;
        bool                              operator!=(BaseProject pro)         const;
        void                              removeRealItemsFor(int id);
        openDocuments                    *getOpenDocuments();
    };
    }
}

#endif // BASEBaseProyect_H
