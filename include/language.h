#ifndef LANGUAGE_H
#define LANGUAGE_H

#include "languageData.h"

#include <map>
#include <dlfcn.h>

namespace pBear
{
    namespace corre
    {
        struct data
        {
            std::vector<std::string> com;
            std::vector<std::string> version;
            unsigned                 defualtCompiler;
        };

        class Language
        {
        private:
            std::map<std::string, LanguageDatas*> dataLanguages;
            std::string                           languagePath;
            static Language *language;

            void load();
            Language();
        public:
            using iterator =  std::map<std::string, LanguageDatas*>::iterator;
            iterator begin();
            iterator end();
        public:
            static Language *instance();
            std::string      getIcon(std::string lan, std::string fileName);
            LanguageDatas    *getLanguage(std::string name);
        };
    }
}

#endif // LANGUAGE_H
