#ifndef BASEDOCUMENT_H
#define BASEDOCUMENT_H

#include "config.h"
//#include "block.h"

#include <string>
#include <map>
#include <vector>
#include <list>

namespace pBear {
    namespace corre {
        //class Block;
      
        class ExceptionNotValidDocumentName: public std::runtime_error 
        {
	public:
	      ExceptionNotValidDocumentName(std::string problem):std::runtime_error(problem){};
	};
        
        class BaseDocument
        {
        public:
            //using iterator = std::list<Block>::iterator;
            
            /*virtual std::string&                     getText() = 0;                                                ///<it return the text of the document
            virtual const std::string&               getName()                         const = 0;                  ///<it return the absolute file path
            virtual bool                             getSaved()                        const = 0;                  ///<it return the saved state;
            virtual const unsigned&                  getVerticalScrollPos()            const = 0;                  ///<it teturn the vertical scroll position
            virtual const unsigned&                  getHorizontalScrollPos()          const = 0;                  ///<it return the horizontal scroll position
            virtual const std::map<int, DOC>        &getEquivalentIDs()                const = 0;                  ///<return the equivalent IDs for the grafic interface
            virtual DOC                              getEquivalentID(int id) = 0;                                  ///<return a equivalent interface id for a specific widget
            virtual bool                             getLoadFlag()                     const = 0;                  ///<return the loaded flag
            //virtual proyect&                         getProyect()                      const = 0;                  ///<return the proyect
            virtual std::string                      getSufix()                        const = 0;                  ///<return the name without the path
            virtual std::string                      getPrefix()                       const = 0;                  ///<return the path without the name
            virtual std::string                      getRelativeNameToPro()            const = 0;                  ///<return a name that are relative to the proyect
            virtual void                             setScrollPos(unsigned&,unsigned&) = 0;                        ///<used to change the scroll pos
            virtual void                             setName(std::string) = 0;                                     ///<used to change the file path
            virtual void                             setText(const std::string &) = 0;                             ///<used to change the text
            virtual void                             setLoadFlag(bool flag) = 0;                                   ///<change the load flag
            //virtual bool                             operator==(BaseDocument&)             const = 0;                  ///<used for know if a document is the same like it;
            virtual void                             setSaved(bool _state) = 0;                                    ///<set a flag that say that a document is saved
            virtual void                             addEquivalentID(int id, DOC _doc) = 0;                        ///<add equivalents a of a equivalent id;
            //virtual void                             setProyect(proyect _pro) = 0;                         ///<set proyect
            virtual void                             read() = 0;                                                   ///<read the file
            virtual bool                             istTextEmpty();                                           ///<get if the text is empty or not
            virtual std::vector<std::pair<std::tuple<int,int, int>, std::tuple<int,int,int>>>  getBraquets() = 0;  ///<get the braquets in the text
            virtual int                              getFirstOfLine(int)                                const = 0;
            virtual int                              getFirstCharacterOfLine(std::tuple<int,int,int>)   const = 0;
            virtual int                              getLineNumberOfPosition(int index)                 const = 0;
            virtual std::string                      getItem()                                          const = 0;
            virtual void                             setItem(std::string itemName) = 0;
            virtual void                             remove() = 0;
            virtual void                             save() = 0;*/
            virtual bool                             operator<(const  BaseDocument &doc)  const = 0;
            virtual bool                             operator>(const  BaseDocument &doc)  const = 0;
            virtual bool                             operator==(const BaseDocument &doc)  const = 0;
            virtual bool                             operator!=(const BaseDocument &doc)  const = 0;
            virtual bool                             constainEquivalentId(DOC doc)   const = 0;
            virtual bool                             hasEquivalentId()               const = 0;
            virtual void                             close() = 0;
            virtual void                             moveToTrush() = 0;
            //virtual std::pair<iterator, int>         setText(int pos, int charsRemoved, int charsAdded, const std::string& text) = 0;
            virtual bool                             empty() = 0;
            //virtual iterator                         findByLine(int line) = 0;
            virtual bool                             renameFile(std::string name) = 0;
        };
    }
}

#endif // BASEDOCUMENT_H
