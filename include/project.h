#ifndef PROYECT_H
#define PROYECT_H

#include "languageData.h"
#include "config.h"
#include "language.h"
#include "BaseProject.h"
#include "realproject.h"

#include <string>
#include <set>
#include <memory>
#include <map>
#include <vector>

namespace pBear
{
    namespace corre
    {
    /**
     * @brief The proyect class is used to represent a proyect
     */
    class Project: public BaseProject
    {
    private:
        std::shared_ptr<RealProject> ptr;


        RealProject *resolve() const;
    public:
        typedef set<Document>::iterator iterator; ///<iterator used to acces the docs
        //..............................
        Project() = default;
        Project(std::string, openDocuments* ops);              ///<constructor used to make a proyect
        Project(const Project &pro);
        Project(std::shared_ptr<RealProject> pro);
        void clear();                                                        ///<destructor necesary to free the doc database (docs)
        iterator                          begin();                           ///<return the begin iterator
        iterator                          end();                             ///<return the end iterator
        std::string                       getName();                         ///<return the name of the proyect
        void                              setName(std::string);              ///<change the name of the proyect
        bool                              elemine(const Document &);         ///<elemine the document using a pointer to the document
        bool                              elemine(iterator);                 ///<elemine the document using a iterator to the document
        iterator                          insert(std::string);               ///<add a document using name
        iterator                          insert(std::string, std::string);  ///<add a document using name and text
        iterator                          insert(Document doc);       ///<add a document using a pointer to this
        iterator                          find(Document &doc);         ///<used to find a document in the proyect
        iterator                          find(DOC doc);
        iterator                          find(std::string name);
        std::string&                      getComand();                       ///<get comand
        void                              setCompileMode(proyCompile mode);  ///<set comand mode
        proyCompile                       getCompileMode();                  ///<get comand mode
        void                              setDefaultHeadersPathes();         ///<set default header pathes
        void                              setDefaultLibrarysPathes();        ///<set Default library pathes
        void                              addHeaderPath(std::string  path);  ///<add header path
        void                              addLibraryPath(std::string path);  ///<add Library path
        std::vector<std::string>          getHeaderPathes();                 ///<get header pathes
        std::vector<std::string>          getLibraryPathes();                ///<get library pathes
        void                              addEquivalentID(int id, PROY _pro); ///<add a equivalent id to the proyect
        PROY                              getEquivalentID(int id);            ///<return the equivalent interface ID for a specific implementation identifiqued by id
        std::map<int, PROY>&              getEquivalentIDs();                 ///<return all proyects
        std::string                       getLanguage();
        void                              setLanguage(std::string language);
        const std::vector<LanguageDatas::Item> &getItems();
        LanguageDatas::Item               getItem(std::string item);
        std::string                       getFileType(std::string name);
        std::string                       getRealToItem(FILEITEMTYPE item);
        FILEITEMTYPE                      getItemToReal(std::string item, int id);
        void                              addItem(std::string name, FILEITEMTYPE item, int id);
        std::vector<FILEITEMTYPE>         getRealItemsForId(int id);
        bool                              hasRealItem(FILEITEMTYPE item);
        bool                              hasItem(std::string itemName)                      const;
        bool                              hasItemForId(int id)                               const;
        void                              remove();
        bool                              isEmpty();
        int                               size();
        void                              saveDocs();
        bool                              containDoc(const Document &doc) const;
        bool                              containDoc(DOC doc)             const;
        bool                              containEquivalentId(PROY)       const;
        std::shared_ptr<RealProject>      getPtr();
        bool                              operator<(const Project& pro)          const;
        bool                              operator>(const Project& pro)          const;
        bool                              operator==(const Project& pro)         const;
        bool                              operator!=(const Project& pro)         const;
        void                              removeRealItemsFor(int id);
        pBear::corre::Document            newDocument(std::string name);
        pBear::corre::Document            newDocument(std::string name, std::string &text);
        pBear::corre::Document            newDocument(std::string name, std::string&& text);
        openDocuments*                    getOpenDocuments();
        UpdateBlockInfo*                  getUpdateBlockInfo();
        void                              setComandStructure(LanguageDatas::ComandStructure comandStructure);
        LanguageDatas::ComandStructure*   getComandStructure();
        operator bool() const;
        template<typename type>
        void                             clearBlockData(std::string name)
        {
            for (Document doc: *this)
            {
                if (!doc) continue;
                doc.clearBlockData<type>(name);
            }
        }
    };
    }
}

#endif // PROYECT_H
