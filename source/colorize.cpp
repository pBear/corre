#include "colorize.h"

using namespace pBear::corre;

Colorize::Colorize(Document doc): doc(doc)
{
}

void Colorize::setFunctionSetColor(std::function<void (int, int, std::string)> funSetColor)
{
    this->funSetColor = funSetColor;
}
