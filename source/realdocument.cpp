#include "realdocument.h"

#include "document.h"
#include "project.h"
#include "pbexeption.h"
#include "event.h"

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <fstream>
#include <stack>
#include <algorithm>
#include <iterator>
#include <list>
//#include <sys/inotify.h>
//#include <sys/select.h>
//#include <errno.h>

using namespace pBear::corre;

//this function need to be very eficient in cpu runtime
/*void RealDocument::updateBrackets(iterator _begin, iterator _end)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    const char *text    = this->text.c_str();
    std::stack<iterator> stack;

    for (iterator it = begin(); it != _begin; ++it)
    {
        for (int i = 0, n = it->getNumEndBrackets(); i < n; i++)
        {
            if (!stack.empty()) stack.pop();
        }
        for (int i = 0, n = it->sizeOpenCloseBrackets(); i < n; i++)
        {
            stack.push(it);
        }
    }

    //_begin - _end
    for (++_end; _begin != _end; ++_begin)
    {
        std::list<int>  fifoOpenCloseBrackets;

        _begin->clearOpenCloseBrackets();
        int coundClose = 0;
        for (int i = _begin->getBegin(), n = _begin->getEnd(); i < n; i++)
        {

            switch (text[i])
            {
                case '{':
                    fifoOpenCloseBrackets.push_back(i);
                    break;
                case '}':
                    if (!fifoOpenCloseBrackets.empty()) fifoOpenCloseBrackets.pop_front();
                    if (fifoOpenCloseBrackets.empty() and !stack.empty())
                    {
                        stack.top()->getOpenCloseBracket(coundClose++).end = &(*_begin);
                        stack.pop();
                    }
                    break;
            };
        }
        for (; fifoOpenCloseBrackets.size(); fifoOpenCloseBrackets.pop_front())
        {
            _begin->addOpenCloseBracket(Block::OpenCloseBracket(), fifoOpenCloseBrackets.front());
            stack.push(_begin);
        }
        _begin->setNumEndBracket(coundClose);
    }

    //after
    int index = 0;
    iterator before = end();
    for (; _end != end() && !stack.empty(); ++_end)
    {
        if (stack.empty()) break;
        for (int i = 0, n = _end->getNumEndBrackets(); i < n; i++)
        {
            if (stack.top() == before) index++;
            else index = 0;
            before = stack.top();
            stack.top()->getOpenCloseBracket(index).end = &(*_end);
            stack.pop();
        }
    }
    for (; !stack.empty(); stack.pop())
    {
        for (int n = stack.top()->sizeOpenCloseBrackets(); index < n; index++)
        {
            stack.top()->getOpenCloseBracket(index).end = nullptr;
        }
        index = 0;
    }
}*/

int RealDocument::findBeginOfBracketBlock(int begin, const int &end)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    const char *text    = this->text.c_str();

    for (int i = begin; i < end; i++)
    {
        if (text[i] != ' ' and text[i] != '\t' and text[i] != '\v')
        {
            return i - begin;
        }
    }
    return end;
}

void RealDocument::checkForErraseBrackets(int pos, int charsRemoved)
{
    std::string &text = this->text;
    for (int n = charsRemoved + pos; pos <= n; pos++)
    {
        if (text[pos] == '{')
        {
            iterator it = find(pos);
            it->setCollapsed(false);
        }
    }
}


/*#define EVENT_SIZE          ( sizeof (struct inotify_event) )
#define EVENT_BUF_LEN       ( 1024 * ( EVENT_SIZE + NAME_MAX + 1) )

void RealDocument::checkFisicalChanges()
{
    muPrivate.lock();
    if (thr)
    {
        int  wd = inotify_add_watch(idInotify, getName().c_str(), IN_ALL_EVENTS );
        if (wd == -1)
        {
            std::cout << name << std::endl;
            std::cout << "inotify_add_watch: " << errno << "END" << std::endl;
        }
        muPrivate.unlock();
    }
    std::call_once(flag1, [this]()
    {
        thr = std::make_unique<std::thread>([this]()
        {
            idInotify = inotify_init();
            fd_set rfds;
            char buffer[EVENT_BUF_LEN];
            int  wd = inotify_add_watch(idInotify, getName().c_str(), IN_ALL_EVENTS );
            if (wd == -1)
            {
                std::cout << name << std::endl;
                std::cout << "inotify_add_watch: " << errno << "END" << std::endl;
            }
            else
            {
                std::cout << "succes" << std::endl;
            }
            while (true)
            {
                FD_ZERO(&rfds);
                FD_SET(idInotify, &rfds);
                muPrivate.unlock();
                int retval = select(idInotify+1, &rfds, NULL, NULL, NULL);
                if (retval < 0) return;

                int length = ::read(idInotify, buffer, EVENT_BUF_LEN);

                for ( int i=0; i < length;)
                {
                    struct inotify_event *event = ( struct inotify_event * ) &buffer[ i ];
                    if (FD_ISSET(idInotify, &rfds))
                    {
                        std::cout << "----------------------------------------------------" << std::endl;
                        std::cout << "IN_ACCESS        = " << std::bitset<32>(IN_ACCESS)  << std::endl;
                        std::cout << "IN_ATTRIB        = " << std::bitset<32>(IN_ATTRIB)  << std::endl;
                        std::cout << "IN_CLOSE_NOWRITE = " << std::bitset<32>(IN_CLOSE_NOWRITE) << std::endl;
                        std::cout << "IN_CLOSE_WRITE   = " << std::bitset<32>(IN_CLOSE_WRITE)  << std::endl;
                        std::cout << "IN_CREATE        = " << std::bitset<32>(IN_CREATE)  << std::endl;
                        std::cout << "IN_DELETE        = " << std::bitset<32>(IN_DELETE) << std::endl;
                        std::cout << "IN_DELETE_SELF   = " << std::bitset<32>(IN_DELETE_SELF)  << std::endl;
                        std::cout << "IN_IGNORED       = " << std::bitset<32>(IN_IGNORED)  << std::endl;
                        std::cout << "IN_ISDIR         = " << std::bitset<32>(IN_ISDIR) << std::endl;
                        std::cout << "IN_MODIFY        = " << std::bitset<32>(IN_MODIFY)  << std::endl;
                        std::cout << "IN_MOVE_SELF     = " << std::bitset<32>(IN_MOVE_SELF)  << std::endl;
                        std::cout << "IN_MOVED_FROM    = " << std::bitset<32>(IN_MOVED_FROM) << std::endl;
                        std::cout << "IN_MOVED_TO      = " << std::bitset<32>(IN_MOVED_TO)  << std::endl;
                        std::cout << "IN_OPEN          = " << std::bitset<32>(IN_OPEN)  << std::endl;
                        std::cout << "IN_Q_OVERFLOW    = " << std::bitset<32>(IN_Q_OVERFLOW) << std::endl;
                        std::cout << "IN_UNMOUNT       = " << std::bitset<32>(IN_UNMOUNT) << std::endl;
                        std::cout << std::endl;
                        std::cout << "IN_UNMOUNT       = " << std::bitset<32>(event->mask) << std::endl;

                        if (event->mask & IN_MODIFY || event->mask & IN_CLOSE_WRITE || event->mask & IN_ATTRIB)
                        {
                            read();
                        }
                        if (event->mask & IN_MOVED_TO)
                        {

                        }
                    }
                    i += EVENT_SIZE + event->len;
                }
            }
        });
    });
}*/

void RealDocument::hideSeeBlocks(Block block)
{
    iterator itBegin = findByLine(block.getLine());
    iterator itEnd   = findByLine(block.getEndBlock()->getLine());
    bool     hide    = block.getCollapsed();
    std::stack<iterator> st;

    itBegin++;


    for (; itBegin != itEnd; itBegin++)
    {
        if (!st.empty() and st.top()->getEndBlock()->getLine() == itBegin->getLine())
        {
            st.pop();
        }
        for (auto x: docHide)
        {
            if (st.empty())
            {
                x.first(itBegin, hide);
            }
        }
        if (itBegin->getCollapsed())
        {
            st.push(itBegin);
        }
    }
}

/*void RealDocument::updateDistanceBracekts(int line, bool mode)
{
    iterator it = findByLine(line);
    int normal_distance = it->getRealOpenCloseBracket().end->getLine() -  it->getLine();

    iterator itl = it;
    for (; itl != begin() and !(itl->sizeOpenCloseBrackets() and itl->getRealOpenCloseBracket().end->getLine() <= it->getLine()); --itl)
    {
        if (itl->hasRealOpenCloseBracket())
        {
            if (mode)
            {
                itl->setDiferenceDistance(-normal_distance);
            }
            else
            {
                itl->setDiferenceDistance(+normal_distance);
            }
        }
    }
    if (itl == begin() and itl->hasRealOpenCloseBracket())
    {
        if (mode)
        {
            itl->setDiferenceDistance(-normal_distance);
        }
        else
        {
            itl->setDiferenceDistance(+normal_distance);
        }
    }

    iterator end   = findByLine(it->getRealOpenCloseBracket().end->getLine());

    int ignore = 0;
    for (++it; it != end; ++it)
    {
        if (it->getCollapsed())
        {
            ignore = it->getRealOpenCloseBracket().end->getLine() - it->getLine();
        }
        if (ignore <= 0)
        {
            if (mode)
            {
                if (!it->getCollapsed())
                {
                    it->setCollapseHide(true);
                }
            }
            else
            {
                it->setCollapseHide(false);
            }
        }
        ignore--;
    }
}*/

RealDocument::iterator RealDocument::begin()
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    return blocks.begin();
}

RealDocument::iterator RealDocument::end()
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    return blocks.end();
}

RealDocument::iterator RealDocument::find(int pos)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    iterator it = std::find_if(begin(), end(), [&](const Block &bl)
    {
        return (bl.getBegin() <= pos and bl.getEnd() >= pos) or (bl.getBegin() > bl.getEnd());
    });
    if (!blocks.empty())
    {
        if (it == end()) return --it;
        return it;
    }
    return end();
}

void RealDocument::eraseBlocks(iterator begin, int pos, int size)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    /*if (begin == end())
    {
        _pbExeption << "eraseBlocks(iterator begin, int pos, int size)";
        _pbExeption << "pBear::corre::RealDocument::";
        _pbExeption << "not valit errase ";
        throw _pbExeption;
    }*/
    int cound = std::count(text.begin() + pos, text.begin() + pos + size, '\n');

    ++begin;
    iterator end = begin;
    std::advance(end, cound);

    if (cound != 0)
    {
        blocks.erase(begin, end);
    }
}

RealDocument::iterator RealDocument::getBefore(Document::iterator it)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    iterator tmp = it; --tmp;
    return tmp;
}

void RealDocument::updateBlock(RealDocument::iterator it)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    iterator before   = it; --before;
    int      distance = it->getEnd() - it->getBegin();

    it->update(before->getEnd() + 1,
               before->getEnd() + 1 + distance,
               before->getLine() + 1);
}

int RealDocument::getNextLineEnd(unsigned pos)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    while (pos < text.size() && text[pos] != '\n') pos++;
    return pos;
}

RealDocument::RealDocument()
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::corre::RealDocument::RealDocument()");
    setSaved(true);
}

RealDocument::RealDocument(std::string nam)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::corre::RealDocument::RealDocument(std::string nam)");
    setName(nam);
    setSaved(true);
    //checkFisicalChanges();
}

RealDocument::RealDocument(string &name, string& text)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::corre::RealDocument::RealDocument(string &name, string& text)");
    setName(name);
    setSaved(true);
    setText(text);
    //checkFisicalChanges();
}

RealDocument::~RealDocument()
{
    if (thr)
    {
        pthread_cancel(thr->native_handle());
        thr->join();
    }
    if (pro)
    {
        //pro->elemine(*this);
    }
}

std::string&    RealDocument::getText()
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getText()");
    return text;
}

const std::string&         RealDocument::getName() const
{
    //std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getName() const");
    return name;
}

bool RealDocument::getSaved() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getSaved() const");
    return saved;
}

const unsigned&  RealDocument::getVerticalScrollPos() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getVerticalScrollPos() const");
    return verticalScroll;
}

const unsigned&  RealDocument::getHorizontalScrollPos() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getHorizontalScrollPos() const");
    return horizonalScroll;
}

const std::map<int, DOC>& RealDocument::getEquivalentIDs() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getEquivalentIDs() const");
    return equivalentsIDs;
}

DOC RealDocument::getEquivalentID(int id)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getEquivalentID(int id)");
    if (equivalentsIDs.find(id) == equivalentsIDs.end())
    {
        return nullptr;
    }
    return equivalentsIDs[id];
}

pBear::corre::Project&  RealDocument::getProyect() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getProyect() const");
    return *pro;
}

bool      RealDocument::getLoadFlag() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getLoadFlag() const");
    return loaded;
}

std::string RealDocument::getSufix() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getSufix() const");
    boost::smatch m;

    boost::regex_search(name, m, boost::regex("/([^/]*)$"));

    if (m.size() <= 1)
    {
        return "";
    }
    return m.str(1);
}
std::string RealDocument::getPrefix() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getPrefix() const");
    boost::smatch m;

    boost::regex_search(name, m, boost::regex("^/([^/]*/)*"));

    if (m.size() <= 1)
    {
        return "";
    }
    return m.str(0).substr(0, m.str(0).size() - 1);
}

void      RealDocument::setScrollPos(unsigned& v, unsigned& h)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::setScrollPos(unsigned& v, unsigned& h)");
    verticalScroll  = v;
    horizonalScroll = h;
}

void      RealDocument::setName(string file)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::setName(string file)");
    
    if (!file.empty() and !boost::filesystem::exists(file))
    {
	throw ExceptionNotValidDocumentName(file + " not exists");
    }
    name            = file;
    //checkFisicalChanges();
}

std::pair<RealDocument::iterator, RealDocument::iterator>      RealDocument::setText(const std::string& _text)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::setText(std::string _text)");

    if (_text == text) return {begin(), begin()};

    blocks.clear();

    return setText(0, text.size(), _text.size(), _text);
}

void      RealDocument::setLoadFlag(bool flag)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::setLoadFlag(bool flag)");
    loaded = flag;
    if (!flag)
    {
        if (pro.get() and getProyect().getOpenDocuments() and getProyect().getOpenDocuments()->getEvent())
        {
            getProyect().getOpenDocuments()->getEvent()->executeFuncDocUnload(Document(doc.lock()));
        }
    }
    else
    {
        if (pro.get() and getProyect().getOpenDocuments() and getProyect().getOpenDocuments()->getEvent())
        {
            getProyect().getOpenDocuments()->getEvent()->executeFuncDocLoad(Document(doc.lock()));
        }
    }
}

bool      RealDocument::operator==(RealDocument& doc) const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::operator==(document& doc) const");
    return this == &doc;
}

void      RealDocument::setSaved(bool _state)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::setSaved(bool _state)");
    if (_state != saved)
    {
      saved = _state;
      if (pro and getProyect().getOpenDocuments() and getProyect().getOpenDocuments()->getEvent())
      {
	getProyect().getOpenDocuments()->getEvent()->executeFuncSaveDoc(doc.lock());
      }
    }
}

void      RealDocument::addEquivalentID(int id, DOC _doc)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::addEquivalentID(int id, DOC _doc)");
    equivalentsIDs[id] = _doc;
}

void      RealDocument::setProyect(Project _pro)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::setProyect(pBear::proyect *_pro)");
    if (pro && _pro == *pro) return;
    pro = std::unique_ptr<Project>(new Project(_pro));
    std::cout << doc.lock().get() << std::endl;
    _pro.insert(doc.lock());
    //pro->insert(*this); IMPOERTANT!!!!!
}

std::string RealDocument::getRelativeNameToPro() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getRelativeNameToPro() const");
    int length = name.find(pro->getName().c_str());

    #pragma GCC diagnostic ignored "-Wsign-compare"
    if (length == std::string::npos)
    {
        return getName();
    }
    else
    {
        return getName().substr(pro->getName().size() + 1);
    }
}

std::pair<RealDocument::iterator, RealDocument::iterator> RealDocument::read()
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::read()");
    std::string tmp;
    std::ifstream in(getName());
    tmp.assign(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>());
    in.close();
    auto ret  = setText(tmp);
    setSaved(true);
    
    if (getUpdateBlockInfo())
    {
        getUpdateBlockInfo()->update(ret.first, ret.second, doc.lock());
    }
    return ret;
}

bool RealDocument::istTextEmpty()
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::istTextEmpty()");
    return text.empty();
}

std::vector<std::pair<std::tuple<int,int, int>, std::tuple<int,int,int>>> RealDocument::getBraquets()
{
    //performance critical
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getBraquets()");
    std::vector<std::pair<std::tuple<int,int, int>, std::tuple<int,int,int>>> vec;
    std::stack<std::tuple<int,int,int>>                                       ele;
    int                                                                       line    = 0;
    int                                                                       linePos = 0;



    const char *text = this->text.c_str();
    for (int i = 0, n = this->text.size(); i < n; i++)
    {
            if (text[i] == '\n')
            {
                line++;
                linePos = i;
            }
            if  (text[i] == '{')
            {
                ele.push(std::make_tuple(line, i - linePos, i));
            }
            if (text[i] == '}' and !ele.empty())
            {
                vec.push_back({ele.top(), std::make_tuple(line, i - linePos, i)});
                ele.pop();
            }
    }
    return vec;
}

int RealDocument::getFirstOfLine(int index) const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getFirstOfLine(int index) const");
    if (index >= text.size())
    {
        _pbExeption << "pBear::document";
        _pbExeption << "getFirstOfLine(int index) const";
        _pbExeption << "the paremaeter index is to hight: " + std::to_string(index);
        throw _pbExeption;
    }
    for (int i = index; i >= 0; i--)
    {
        if (text[i] == '\n')
        {
            return i + 1;
        }
    }
    return 0;
}

int RealDocument::getFirstCharacterOfLine(std::tuple<int,int,int> index) const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::quitTab()");
    for (int i = getFirstOfLine(std::get<2>(index)), n = text.size(); i < n; i++)
    {
        if (text[i] != ' ' and text[i] != '\t')
        {
            return std::get<1>(index) + i - std::get<2>(index) - 1;
        }
    }
    return 0;
}

int RealDocument::getLineNumberOfPosition(int index) const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getLineNumberOfPosition(int index) const");
    if (index >= text.size())
    {
        _pbExeption << "getLineNumberOfPosition(int index) const";
        _pbExeption << "pBear::document";
        _pbExeption << "the paremaeter doc has a empty name";
        throw _pbExeption;
    }

    int returnValue = 0;
    for (int i = 0; i <= index; i++)
    {
        if (text[i] == '\n')
        {
            returnValue++;
        }
    }
    return returnValue;
}

std::string RealDocument::getItem() const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::getItem() const");
    return  itemName;
}

void        RealDocument::setItem(std::string itemName)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    _pbExeption.addFunction("pBear::document::setItem(std::string itemName)");
    
    if (this->itemName != itemName)
    {
	  this->itemName = itemName;
	  if (pro and getProyect().getOpenDocuments() and getProyect().getOpenDocuments()->getEvent())
	  {
	      getProyect().getOpenDocuments()->getEvent()->executeFuncUpdateDoc(doc.lock());
	  }
    }
}

void       RealDocument::remove()
{
    std::lock_guard<std::recursive_mutex> lock(mu);
}

void       RealDocument::save()
{
    if (saved) return;
    
    std::lock_guard<std::recursive_mutex> lock(mu);
    std::ofstream of(name, std::ofstream::out | std::ofstream::trunc);
    of << text;
    of.close();
    setSaved(true);
}

bool        RealDocument::operator<(const BaseDocument &doc) const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    return this < &doc;
}

bool        RealDocument::operator>(const BaseDocument &doc) const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    return this > &doc;
}

bool        RealDocument::operator==(const BaseDocument &doc) const
{
    //std::lock_guard<std::recursive_mutex> lock(mu);
    return this == &doc;
}

bool        RealDocument::operator!=(const BaseDocument &doc) const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    return this != &doc;
}

bool        RealDocument::constainEquivalentId(DOC doc)        const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    for (std::pair<int, DOC> x: equivalentsIDs)
    {
        if (x.second == doc)
        {
            return true;
        }
    }
    return false;
}

bool        RealDocument::hasEquivalentId()               const
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    return !equivalentsIDs.empty();
}

void        RealDocument::close()
{
    std::lock_guard<std::recursive_mutex> lock(mu);

    if (pro->getOpenDocuments() && pro->getOpenDocuments()->getEvent())
    {
        pro->getOpenDocuments()->getEvent()->executeFuncDocClose(Document(doc.lock()));
    }

    pro->elemine(Document(doc.lock()));
}

void        RealDocument::moveToTrush()
{
    std::lock_guard<std::recursive_mutex> lock(mu);

    system(("gvfs-trash " + name).c_str());

    close();
}

//(int pos, int charsRemoved, int charsAdded, const std::string& text)
//(int pos, const std::string& replaceText, const std::string& newText)
std::pair<RealDocument::iterator, RealDocument::iterator> RealDocument::setText(int pos, int charsRemoved, int charsAdded, const std::string& text)
{
    std::lock_guard<std::recursive_mutex> lock(mu);

    if (text == this->text)
    {
        std::cerr << "text no change" << std::endl;
        return {begin(), begin()};
    }

    //find the correponding block to the position
    iterator it = find(pos);


    //elemine
    eraseBlocks(it, pos, charsRemoved);

    //checkForErraseBrackets(pos, charsRemoved);

    //set the text
    this->text = text;

    //add
    if (blocks.empty())
    {
        it = blocks.insert(end(), Block(0, 0, 0, doc));
    }

    //add the blocks
    int cur   = pos;
    int cound = 0;
    if (charsAdded)
    {
        cound = std::count(text.begin() + pos, text.begin() + pos + charsAdded, '\n');
    }
    cur = getNextLineEnd(pos);
    it->update(it->getBegin(), cur, it->getLine());

    it++;

    for (int i = 0; i < cound; i++)
    {
        blocks.insert(it, Block(getBefore(it)->getEnd() + 1, (cur = getNextLineEnd(cur + 1)), getBefore(it)->getLine() + 1, doc));
    }


    for (; it != end(); ++it)
    {
        updateBlock(it);
    }

    iterator begin = find(pos);
    iterator end   = begin;
    std::advance(end, cound);
    //updateBrackets(begin, end);

    //set saved state to unsaved

    if (ana)
    {
        ana->run();
    }

    getUpdateBlockInfo()->update(begin, end, Document(doc.lock()));

    for (auto x: *this)
    {
        if (x.getLine() > 100)
        {
            std::cerr << "line to higth:::::::::::::::::::::::::::::::::" << std::endl;
        }
    }

    std::cout << "::::::" << cound << std::endl;
    std::cout << "::::::" << charsAdded << std::endl;
    std::cout << "-------------------LINES------------------" << std::endl;
    for (auto x: *this)
    {
	std::cout << x.getLine() << std::endl;
    }
    std::cout << "----------------------------" << std::endl;
    
    if (getProyect() && getProyect().getOpenDocuments() && getProyect().getOpenDocuments()->getEvent())
    {
        std::cout << "update gui" << std::endl;
        getProyect().getOpenDocuments()->getEvent()->executeFuncDocUpdate(doc.lock(), find(pos), pos, charsRemoved, charsAdded, text.substr(pos, charsAdded));
    }
    //save();

    setSaved(false);
    
    return {begin, end};
}

bool RealDocument::empty()
{
    return blocks.empty();
}

RealDocument::iterator RealDocument::findByLine(int line)
{
    iterator it = begin();
    std::advance(it, line);



    return it;
}

bool RealDocument::renameFile(std::string name)
{
    _pbExeption.addFunction("pBear::corre::RealDocument::renameFile(std::string name)");

    if (boost::filesystem::exists(name))
    {
        return false;
    }

    boost::filesystem::rename(name, getName());
    return true;
}

void RealDocument::updateLanguage()
{
        auto fun = [this](std::vector<LanguageDatas::Item> &items)
        {
            return  std::find_if(items.begin(), items.end(), [this](LanguageDatas::Item item)
            {
                for (std::string ex: item.extencions)
                {
                    if (boost::regex_search(name, boost::regex(ex + "$")))
                    {
                        return true;
                    }
                }
                return false;
            });
        };

        std::vector<LanguageDatas::Item> items = getProyect().getItems();
        auto it = fun(items);

        if (it != items.end() && !it->name.empty())
        {
            setItem(it->name);
            setAnalizer(Language::instance()->getLanguage(getProyect().getLanguage())->getAnalizer(getProyect().getOpenDocuments()->getEvent(), doc.lock()));
        }
        else
        {
            for (auto x: *Language::instance())
            {
                LanguageDatas *lanData = x.second;
                auto items = lanData->getItems();
                auto it = fun(items);
                if (it != items.end())
                {
                    setItem(it->name);
                    setAnalizer(Language::instance()->getLanguage(x.first)->getAnalizer(getProyect().getOpenDocuments()->getEvent(), doc.lock()));
                    break;
                }
            }
        }

    for (Block x: *this)
    {
        x.updateLanguage();
    }

    if (getProyect().getUpdateBlockInfo())
    {
        getProyect().getUpdateBlockInfo()->update(begin(), end(), doc.lock());
    }
}

Analizer *RealDocument::getAnalizer()
{
    return ana.get();
}

void RealDocument::setAnalizer(std::shared_ptr<Analizer> ana)
{
    this->ana = ana;
    ana->addFunction(std::bind(&pBear::corre::RealDocument::updateDocAnalizer, this), this);
}

void RealDocument::updateDocAnalizer()
{
    std::vector<DiagnosticData> datas = ana->getDiagnostics();
    for (auto it = begin(); it != end(); ++it)
    {
        std::list<DiagnosticData> tmp;
        for (DiagnosticData y: datas)
        {
            if (y.line == it->getLine())
            {
                tmp.push_back(y);
            }
        }
        it->setDiagnostics(tmp);
     }

    for (auto x: funcsAna)
    {
        x.first();
    }
}

void RealDocument::setDoc(std::weak_ptr<RealDocument> doc)
{
    this->doc = doc;
    std::cout << doc.lock().get() << " " << this << std::endl;
}

void RealDocument::addFunctionAna(std::function<void ()> fun, void *pointer)
{
    funcsAna.push_back({fun,pointer});
}

void RealDocument::quitFunctionAna(void *pointer)
{
    for (auto it = funcsAna.begin(); it != funcsAna.end(); it++)
    {
        if (it->second == pointer)
        {
            funcsAna.erase(it);
            return;
        }
    }
}

void RealDocument::addFunctionHide(std::function<void (RealDocument::iterator, bool)> fun, void *pointer)
{
    docHide.push_back({fun, pointer});
}

void RealDocument::quitFunctionHide(void *pointer)
{
    for (int i = 0; i < docHide.size(); i++)
    {
        if (docHide[i].second == pointer)
        {
            docHide.erase(docHide.begin() + i);
            return;
        }
    }
}

void RealDocument::setType(RealDocument::Type type)
{
    this->type = type;
}

RealDocument::Type RealDocument::getType()
{
    return type;
}

std::string RealDocument::getExtension()
{
    boost::smatch m;

    boost::regex_search(name, m, boost::regex(R"(\.([^.]*)$)"));

    if (m.size() <= 1)
    {
        return "";
    }
    return m.str(1);
}

string RealDocument::getLanguage()
{
    /*if (getProyect())
    {
      for (LanguageDatas::Item x: getProyect().getItems())
      {
	if (x.name == getItem()) return getProyect().getLanguage();
      }
    }*/
    
    for (auto x: *Language::instance())
    {
      for (LanguageDatas::Item y: x.second->getItems())
      {
	if (y.name == getItem()) return x.second->getName();
      }
    }
    
    return "";
}

std::shared_ptr< UpdateBlockInfo > RealDocument::getUpdateBlockInfo()
{
    return Language::instance()->getLanguage(getLanguage())->getUpdateBlockInfo();
}

void RealDocument::rename(std::string newName)
{
    int pos = getProyect().getName().find(newName);
    std::string final_new_name;
    
    if (pos == std::string::npos)
    {
	final_new_name = name;
    }
    else
    {
	final_new_name = getProyect().getName() + 
			(getProyect().getName().back() == '/'? "": "/") +
			    newName;
    }
    
    if (final_new_name == name) return;
    
    renameFile(final_new_name);
    setName(final_new_name);
}

