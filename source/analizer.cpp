﻿#include "analizer.h"

#include "event.h"

#include <string>
#include <thread>
#include <sstream>
#include <iostream>
#include <unistd.h>
#include <algorithm>

using namespace pBear::corre;

void Analizer::realRun()
{
    while (!end)
    {
        do
        {
            state = true;
            usleep(900);
        }
        while (!state);

        analizeText();
        mu.lock();
        if (enable&&DIAGNOSTIC == DIAGNOSTIC)
        {
            updateDiagnostic();
        }
        if (enable&&TOKONIZE == TOKONIZE)
        {
            updateTokens();
        }
        mu.unlock();
        event->executeFuncAnalizerReady(doc);
        for (auto x: funcs)
        {
            x.first();
        }
        /*if (pause.try_lock())
        {
            pause.lock();
        }*/
        pause.lock();
    }
}

void Analizer::run()
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    state = false;
    bool bl = true;
    std::call_once(flag, [&]()
    {
        bl = false;
        th = std::make_unique<std::thread>(&pBear::corre::Analizer::realRun, this);
    });
    if (bl)
    {
        pause.unlock();
    }
}

Analizer::Analizer(Event *event, Document doc):
    doc(doc), event(event), end(false)
{
}

Analizer::~Analizer()
{
    end = true;
    mu.unlock();
    pause.unlock();
    if (th)
    {
        th->join();
    }
}

void Analizer::addFunction(std::function<void ()> fun, void *pointer)
{
    funcs.push_back({fun,pointer});
}

void Analizer::quitFunction(void *pointer)
{
    for (auto it = funcs.begin(); it != funcs.end(); it++)
    {
        if (it->second == pointer)
        {
            funcs.erase(it);
            return;
        }
    }
}


void Analizer::setAnalizeData(Enable _enable, unsigned _line, unsigned _colum, unsigned _offset)
{
    std::lock_guard<std::recursive_mutex> lock(mu);
    enable = _enable;
    line   = _line;
    colum  = _colum;
    offset = _offset;
}

//thread

void Analizer::blockThread()
{
    mu.lock();
}

void Analizer::unlockThread()
{
    mu.unlock();
}

std::vector<tokenData> &Analizer::getTokens()
{
    return tokens;
}

std::vector<DiagnosticData> &Analizer::getDiagnostics()
{
    return diagnostics;
}

