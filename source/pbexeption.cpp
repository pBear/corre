#include "pbexeption.h"

#include <iostream>
#include <sstream>
#include <thread>

namespace pBear
{
pbExeption _pbExeption;

}

std::mutex              mu;

using namespace pBear;

//pbExeption _pbExeption;

void pbExeption::setData(std::string _functionName, std::string _className, std::string _extraInfo) noexcept
{
    functionName = _functionName;
    className    = _className;
    extraInfo    = _extraInfo;
}

const char* pbExeption::what() const noexcept
{
    std::ostringstream out;

    std::queue<std::string> q = lastFunctions;
    while (!q.empty())
    {
        out << q.front() << std::endl;
        q.pop();
    }

    out << std::endl;

    if (className.empty())
        out << "ERROR: problem with function " << functionName;
    else
        out << "ERROR: problem with the method " << functionName << " in class named " << className;

    out << std::endl << "     info: " << extraInfo << std::endl;

    return out.str().c_str();
}

pbExeption&  pbExeption::operator<<(std::string str)
{
    if (index == 0)
    {
        functionName = str;
            index++;
        return *this;
    }
    if (index == 1)
    {
        className = str;
        index++;
        return *this;
    }
    extraInfo += str;
    return *this;
}

pbExeption& pbExeption::operator<<(void *pointer)
{
    std::ostringstream os;
    os << pointer;
    extraInfo += os.str();
    extraInfo += "\0";
    return *this;
}

void        pbExeption::addFunction(std::string func)
{
    std::lock_guard<std::mutex> lock(mu);
    if (lastFunctions.size() > 6)
    {
        lastFunctions.pop();
    }
    lastFunctions.push(func);
}
