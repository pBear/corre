#include "languageData.h"

#include "project.h"

#include <regex>

using namespace pBear::corre;

std::string LanguageDatas::getName()
{
    return name;
}

pBear::corre::Document LanguageDatas::addDocument(pBear::corre::Project pro)
{
    Document doc;// = pro.newDocument(pro.getName() + "/" + pro.getComandStructure()->file);
    //doc.setText(pro.getComandStructure()->defaultText);
    //doc.setType(Document::Type::CONFIG);
    return doc;
}

std::string transform(std::string str, std::string exe, std::string path)
{
    str = std::regex_replace(str, std::regex("<executable>"), exe);
    return std::regex_replace(str, std::regex("<path>"), path);
}

std::string LanguageDatas::ComandStructure::getCompileSTR()
{
    return transform(compile[compileCurrent].second, executable, path);
}

std::string LanguageDatas::ComandStructure::getDebugSTR()
{
    return transform(debug[debugCurrent].second, executable, path);
}

std::string LanguageDatas::ComandStructure::getExecuteSTR()
{
    return transform(execute[executeCurrent].second, executable, path);
}



