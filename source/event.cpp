﻿#include "event.h"
#include "pbexeption.h"
#include "BaseProject.h"


#include <algorithm>
#include <iostream>

pBear::corre::Event  _Event;

std::string path_executable;

using namespace pBear::corre;

Event::Event()
{
    ops.setEvent(this);
}


void Event::executeFunc(const _functions_ter_ &funcs, unsigned term)
{
    std::for_each(funcs.begin(), funcs.end(), [&term](_function_ter_ fun)
    {
        fun(term);
    });
}

openDocuments *Event::getOpenDocuments()
{
    return &ops;
}

void Event::executeFuncDebuge(unsigned term)
{
    executeFunc(debuge, term);
}

void Event::executeFuncCloseTerm(unsigned term)
{
    executeFunc(closeTerm, term);
}

void Event::executeFuncAddProyect(PROY pro)
{
    std::for_each(addPro.begin(), addPro.end(), [&pro](_function_pro_ pr)
    {
        pr(pro);
    });
}

void Event::executeFuncRegularInvoque()
{
    for(dataFunRegularInvoque x: funcsRegularInvoque)
    {
        x.fun();
    }
}

void Event::executeFuncLongRegularInvoque()
{
    std::for_each(funcsLongRegularInvoque.begin(), funcsLongRegularInvoque.end(), [](_function_void_ func)
    {
        func();
    });
}

void Event::executeFuncChangeCurDoc(Document _doc)
{
    //_openDocuments->setCurrentDoc(_doc);
    for (_functon_doc_ doc: funcsChangeCurDoc)
    {
        doc(_doc);
    }
}

void Event::executeFuncNewProyect(Project pro)
{
    std::cout << funcsAddProy.size() << std::endl;
    std::for_each(funcsAddProy.begin(), funcsAddProy.end(),
        [&pro](std::function<void(Project)> fun)
        {
           fun(pro);
        });
}

void Event::executeFuncNewDoc(Document &doc)
{
    docData data;
    std::for_each(funcsAddDoc.begin(), funcsAddDoc.end(), [&doc, &data](_function_doc_add_ fun)
    {
        data = fun(doc);
        doc.addEquivalentID(data.id, data.data);
    });

    for (auto fun: funcsNormalAddDocument)
    {
      fun(doc);
    }
    //executeFuncChangeCurDoc(doc.getEquivalentID(data.id));
}

void Event::executeFuncChangeCurProy(Project &pro)
{
    //for compatible resons, it will be deleated
    //_openDocuments->setCurrentPro(pro.getEquivalentID(0));

    for (_function_pro_cur_change fun: funcsProChange)
    {
        fun(pro);
    }
}

void Event::executeFuncProUpdate(Project pro)
{
    for (std::function<void(Project)> fun: funcsProUpdate)
    {
        fun(pro);
    }
}

void Event::executeFuncDocUpdate(pBear::corre::Document doc, corre::Document::iterator it, int pos, int remove, int add,std::string text)
{
    for (auto fun: funcsDocUpdate)
    {
        fun(doc, it, pos, remove, add, text);
    }
}

void Event::executeFuncDocLoad(Document doc)
{
    for (auto x: funcsDocLoad)
    {
        x(doc);
    }
}



void Event::executeFuncDocClose(Document doc)
{
    for (auto x: funcsDocClose)
    {
        x(doc);
    }
    doc.getProyect().elemine(doc);
}

void Event::executeFuncDocUnload(Document doc)
{
    for (auto x: funcsDocUnload)
    {
        x(doc);
    }
}

void Event::executeFuncDocMoveToTrush(Document doc)
{
    for (_functon_doc_ fun: funcsDocMoveToTrush)
    {
        fun(doc);
    }
    std::string name = doc.getName();
    doc.close();
    system(("gvfs-trash " + name).c_str());
}

void Event::executeFuncEditTextFocusChanged(identity index, Document doc)
{
    for (auto x: funcsFocusEditTextChanged)
    {
        x(index, doc);
    }
    executeFuncChangeCurDoc(doc);
}

void Event::executeFuncChangeTab(Document doc, TABID tab)
{
    for (auto x: funcsTabChanged)
    {
        x(doc, tab);
    }
}

void Event::executeFuncAddTab(Document doc, TABID tab)
{
    for (auto x: funcsTabAdd)
    {
        x(doc, tab);
    }
}

void Event::executeFuncCloseTab(Document doc, TABID tab)
{
    for (_function_tab_close_ x: funcsTabClose)
    {
        x(doc, tab);
    }
}

void Event::executeFuncLanguageChanged(Project pro)
{
    //executeFunctionLanguageChangedDialog(pro);
    for (_function_laguage_changed_ fun: funcsLanguageChanged)
    {
        fun(pro);
    }
}

void Event::executeFunctionLanguageChangedDialog(Project pro)
{
    functionLanguageChangedDialog(pro);
}

void Event::executeFuncLanguageLoad()
{
    for (_function_void_ fun: funcsLanguageLoad)
    {
        fun();
    }
}

void Event::executeFuncHideBlocks(unsigned firstLine, unsigned lastLine, bool flag)
{
    for (_function_hide_blocks_ fun :funcsHideBlocks)
    {
        fun(firstLine, lastLine, flag);
    }
}

void Event::executeFuncAnalizerReady(Document doc)
{
    for (auto x: funcsAnalizerReady)
    {
        x(doc);
    }
}

void Event::executeFuncLocalTermainalBetThreads()
{
    if (!executed)
    {
        for (_func_execute_local_term_ fun :funcsExecuteLocalTerm)
        {
            fun(uniqueID, comand);
        }
        executed = true;
    }
}

void Event::executeFuncLocalTermainal(int uniqueID, std::string comand)
{
    this->uniqueID = uniqueID;
    this->comand   = comand;
    executed       = false;

    /*for (_func_execute_local_term_ fun :funcsExecuteLocalTerm)
    {
        fun(uniqueID, comand);
    }*/
}


void Event::addFunNewProyect(std::function<void (pBear::corre::Project)> fun)
{
    addFunction(fun, funcsAddProy);
}


void Event::addFunNewDocumentGUI(std::function<pBear::corre::Event::docData (pBear::corre::Document)> fun)
{
    addFunction(fun, funcsAddDoc);
}


void Event::addFuncProUpdate(std::function<void (pBear::corre::Project)> fun)
{
    addFunction(fun, funcsProUpdate);
}


void Event::addFuncDocLoad(std::function<void (pBear::corre::Document)> fun)
{
    addFunction(fun, funcsDocLoad);
}


void Event::addFuncDocClose(std::function<void (pBear::corre::Document)> fun)
{
    addFunction(fun, funcsDocClose);
}


void Event::addFuncDocUnload(std::function<void (pBear::corre::Document)> fun)
{
    addFunction(fun, funcsDocUnload);
}


void Event::addFuncChangeLanguage(std::function<void (pBear::corre::Project)> fun)
{
    addFunction(fun, funcsLanguageChanged);
}


void Event::addFuncDocUpdate(_function_doc_update_ fun)
{
    funcsDocUpdate.push_back(fun);
}

void Event::addFunNewDocument(std::function< void(Document) > fun)
{
    funcsNormalAddDocument.push_back(fun);
}

void Event::addFuncSaveDoc(std::function<void(Document)> fun)
{
  funcsSaveDoc.push_back(fun);
}

void Event::executeFuncSaveDoc(Document doc)
{
    for (auto fun: funcsSaveDoc)
    {
      fun(doc);
    }
}

void Event::addFuncUpdateDoc(std::function<void(Document)> fun)
{
    funcsUpdateDoc.push_back(fun);
}

void Event::executeFuncUpdateDoc(Document doc)
{
    for (auto fun: funcsUpdateDoc)
    {
	fun(doc);
    }
}

void Event::addFuncDocChangeCurrent(std::function< void(Document) > fun)
{
    funcsChangeCurDoc.push_back(fun);
}
