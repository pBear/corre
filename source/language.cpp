#include "language.h"
#include "opendocuments.h"
#include "event.h"

#include <functional>

#include <boost/filesystem.hpp>
#include <iostream>
#include <boost/regex.hpp>

namespace pBear {
    class proyect;
}

using namespace pBear::corre;

//class proyect;


Language *Language::language = nullptr;

void Language::load()
{
    boost::filesystem::directory_iterator end;

    bool changed = false;
    for (boost::filesystem::directory_iterator it(languagePath); it != end; ++it)
    {
        void *hndl = dlopen(it->path().c_str(), RTLD_NOW);
        if(hndl == NULL)
        {
           std::cerr << dlerror() << std::endl;
           _exit(-1);
        }
        std::function<LanguageDatas*()> fun = (LanguageDatas*(*)()) dlsym(hndl, "colorMaker");
        LanguageDatas *dat = fun();
        if (dataLanguages.find(dat->getName()) == dataLanguages.end())
        {
            dataLanguages[dat->getName()] = dat;
        }
    }
    if (changed)
    {
        //event->executeFuncLanguageLoad();
    }
}

Language::iterator Language::begin()
{
    return dataLanguages.begin();
}

Language::iterator Language::end()
{
    return dataLanguages.end();
}

Language *Language::instance()
{
    if (language == nullptr)
    {
        language = new Language;
    }
    return language;
}

Language::Language()
{
    /*_data[pBear::proyType::C].com.push_back("gcc");
    _data[pBear::proyType::C].com.push_back("clang");
    _data[pBear::proyType::C].defualtCompiler = 0;
    _data[pBear::proyType::C].version.push_back("c90");
    _data[pBear::proyType::C].version.push_back("c99");
    _data[pBear::proyType::C].version.push_back("c11");
    _data[pBear::proyType::C].version.push_back("gnu90");
    _data[pBear::proyType::C].version.push_back("gnu99");
    _data[pBear::proyType::C].version.push_back("gnu11");

    _data[pBear::proyType::CPP].com.push_back("g++");
    _data[pBear::proyType::CPP].com.push_back("clang++");
    _data[pBear::proyType::C].defualtCompiler = 0;
    _data[pBear::proyType::C].version.push_back("c++98");
    _data[pBear::proyType::C].version.push_back("c++11");
    _data[pBear::proyType::C].version.push_back("c++98");
    _data[pBear::proyType::C].version.push_back("gnu++98");
    _data[pBear::proyType::C].version.push_back("gnu++11");*/

#ifndef LANGUAGE_PATH
#ifndef ROOT_PROJECT
#error LANGUAGE_PATH or ROOT_PROJECT must be defined (compile with -DLANGUAGE_PATH=...)
#endif
    languagePath =  ROOT_PROJECT + std::string("/languages/libs");
#else
    languagePath = LANGUAGE_PATH;
#endif
    
    load();
}

std::string Language::getIcon(std::string lan, std::string fileName)
{
    if (lan != "")
    {
        for (LanguageDatas::Item x: getLanguage(lan)->getItems())
        {
            for (std::string ex: x.extencions)
            {
                for (int i = ex.size() - 1, j = fileName.size() - 1; i >= 0; i--, j--)
                {
                    if (ex[i] != fileName[j])
                    {
                        break;
                    }
                    else if ( i == 0 and ex[i] == fileName[j])
                    {
                        return x.iconDoc;
                    }
                }
            }
        }
    }
    for (std::pair<std::string, LanguageDatas*> lan: dataLanguages)
    {
        for (LanguageDatas::Item x: lan.second->getItems())
        {
            for (std::string ex: x.extencions)
            {
                for (int i = ex.size() - 1, j = fileName.size() - 1; i >= 0; i--, j--)
                {
                    if (ex[i] != fileName[j])
                    {
                        break;
                    }
                    else if ( i == 0 and ex[i] == fileName[j])
                    {
                        return x.iconDoc;
                    }
                }
            }
        }
    }
    return "";
}

LanguageDatas*            Language::getLanguage(std::string name)
{
    return dataLanguages[name];
}
