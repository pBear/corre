#include "document.h"
#include "project.h"
#include "pbexeption.h"
#include "event.h"

#include <regex>
#include <fstream>
#include <stack>
#include <algorithm>
#include <iterator>
#include <list>

using namespace pBear::corre;

RealDocument *Document::resolve() const
{
    if (!ptr.get())
    {
        _pbExeption << "resolve()";
        _pbExeption << "pBear::corre::document";
        _pbExeption << "can not execute without valid document";
        throw _pbExeption;
    }
    return ptr.get();
}

Document::iterator Document::begin()
{
    return resolve()->begin();
}

Document::iterator Document::end()
{
    return resolve()->end();
}

Document::iterator Document::find(int pos)
{
    return resolve()->find(pos);
}

Document::Document(std::shared_ptr<RealDocument> ptr)
{
    this->ptr = ptr;
}

Document::Document(std::string nam)
{
    ptr = std::make_shared<RealDocument>(nam);
    ptr->setDoc(ptr);
}

Document::Document(string &name, string& text)
{
    ptr = std::make_shared<RealDocument>(name, text);
    ptr->setDoc(ptr);
}

Document::Document(const Document& doc)
{
    ptr = doc.ptr;
}

Document::~Document()
{

}

std::string&    Document::getText()
{

    return resolve()->getText();
}

const std::string&         Document::getName() const
{
    return resolve()->getName();
}

bool Document::getSaved() const
{
    return resolve()->getSaved();
}

const unsigned&  Document::getVerticalScrollPos() const
{
    return resolve()->getVerticalScrollPos();
}

const unsigned&  Document::getHorizontalScrollPos() const
{
    return resolve()->getHorizontalScrollPos();
}

const std::map<int, DOC>& Document::getEquivalentIDs() const
{
    return resolve()->getEquivalentIDs();
}

DOC Document::getEquivalentID(int id)
{
    return resolve()->getEquivalentID(id);
}

Project& Document::getProyect() const
{
    return resolve()->getProyect();
}

bool      Document::getLoadFlag() const
{
    return resolve()->getLoadFlag();
}

std::string Document::getSufix() const
{
    return resolve()->getSufix();
}
std::string Document::getPrefix() const
{
    return resolve()->getPrefix();
}

std::shared_ptr<RealDocument> Document::getPtr() const
{
    return ptr;
}


void      Document::setScrollPos(unsigned& v, unsigned& h)
{
    resolve()->setScrollPos(v, h);
}

void      Document::setName(std::string file)
{
    resolve()->setName(file);
}

std::pair<Document::iterator, Document::iterator>      Document::setText(const std::string& _text)
{
   return resolve()->setText(_text);
}

void      Document::setLoadFlag(bool flag)
{
    resolve()->setLoadFlag(flag);
}

bool      Document::operator==(Document& doc) const
{
    return *ptr == *doc.ptr;
}

void      Document::setSaved(bool _state)
{
    resolve()->setSaved(_state);
}

void      Document::addEquivalentID(int id, DOC _doc)
{
    resolve()->addEquivalentID(id, _doc);
}

void      Document::setProyect(Project _pro)
{
    resolve()->setProyect(_pro);
}

std::string Document::getRelativeNameToPro() const
{
    return resolve()->getRelativeNameToPro();
}

void Document::read()
{
    std::pair<iterator, iterator> tmp = resolve()->read();
}

bool Document::istTextEmpty()
{
    return resolve()->istTextEmpty();
}

std::vector<std::pair<std::tuple<int,int, int>, std::tuple<int,int,int>>> Document::getBraquets()
{
    return resolve()->getBraquets();
}

int Document::getFirstOfLine(int index) const
{
    return resolve()->getFirstOfLine(index);
}

int Document::getFirstCharacterOfLine(std::tuple<int,int,int> index) const
{
    return resolve()->getFirstCharacterOfLine(index);
}

int Document::getLineNumberOfPosition(int index) const
{
    return resolve()->getLineNumberOfPosition(index);;
}

std::string Document::getItem() const
{
    return resolve()->getItem();
}

void        Document::setItem(std::string itemName)
{
    return resolve()->setItem(itemName);
}

void       Document::remove()
{
    resolve()->remove();
}

void       Document::save()
{
    resolve()->save();
}

bool        Document::operator<(const BaseDocument &doc) const
{
    return *ptr < *static_cast<const Document*>(&doc)->ptr;
}

bool        Document::operator>(const BaseDocument &doc) const
{
    return *ptr > *static_cast<const Document*>(&doc)->ptr;
}

bool        Document::operator==(const BaseDocument &doc) const
{
    return *ptr == *static_cast<const Document*>(&doc)->ptr;
}

/*void Document::updateDistanceBracekts(int line, bool mode)
{
    iterator it = findByLine(line);
    int normal_distance = it->getRealOpenCloseBracket().end->getLine() -  it->getLine();

    iterator itl = it;
    for (; itl != begin() and !(itl->sizeOpenCloseBrackets() and itl->getRealOpenCloseBracket().end->getLine() <= it->getLine()); --itl)
    {
        if (itl->hasRealOpenCloseBracket())
        {
            if (mode)
            {
                itl->setDiferenceDistance(-normal_distance);
            }
            else
            {
                itl->setDiferenceDistance(+normal_distance);
            }
        }
    }
    if (itl == begin() and itl->hasRealOpenCloseBracket())
    {
        if (mode)
        {
            itl->setDiferenceDistance(-normal_distance);
        }
        else
        {
            itl->setDiferenceDistance(+normal_distance);
        }
    }

    iterator end   = findByLine(it->getRealOpenCloseBracket().end->getLine());

    int ignore = 0;
    for (++it; it != end; ++it)
    {
        if (it->getCollapsed())
        {
            ignore = it->getRealOpenCloseBracket().end->getLine() - it->getLine();
        }
        if (ignore <= 0)
        {
            if (mode)
            {
                if (!it->getCollapsed())
                {
                    it->setCollapseHide(true);
                }
            }
            else
            {
                it->setCollapseHide(false);
            }
        }
        ignore--;
    }
}*/

bool        Document::operator!=(const BaseDocument &doc) const
{
    if (!this->getPtr()) return true;
    return *ptr != *static_cast<const Document*>(&doc)->ptr;
}

Document&       Document::operator=(const Document &doc)
{
    ptr = doc.ptr;
    return *this;
}

bool        Document::constainEquivalentId(DOC doc)        const
{
    return resolve()->constainEquivalentId(doc);
}

bool        Document::hasEquivalentId()               const
{
    return resolve()->hasEquivalentId();
}

void        Document::close()
{
    resolve()->close();
}

void        Document::moveToTrush()
{
    return resolve()->moveToTrush();
}

//(int pos, int charsRemoved, int charsAdded, const std::string& text)
//(int pos, const std::string& replaceText, const std::string& newText)
std::pair<Document::iterator, Document::iterator> Document::setText(int pos, int charsRemoved, int charsAdded, const std::string& text)
{
     return resolve()->setText(pos, charsRemoved, charsAdded, text);
}

bool Document::empty()
{
    return resolve()->empty();
}

Document::iterator Document::findByLine(int line)
{
    return resolve()->findByLine(line);
}

bool Document::renameFile(std::string name)
{
    return resolve()->renameFile(name);
}

void Document::updateLanguage()
{
    return resolve()->updateLanguage();
}

Analizer *Document::getAnalizer()
{
    return resolve()->getAnalizer();
}

void Document::addFunctionAna(std::function<void ()> fun, void *pointer)
{
    resolve()->addFunctionAna(fun, pointer);
}

void Document::quitFunctionAna(void *pointer)
{
    resolve()->quitFunctionAna(pointer);
}

void Document::addFunctionHide(std::function<void (Document::iterator, bool)> fun, void *pointer)
{
    resolve()->addFunctionHide(fun, pointer);
}

void Document::quitFunctionHide(void *pointer)
{
    resolve()->quitFunctionHide(pointer);
}

void Document::hideSeeBlocks(Block block)
{
    resolve()->hideSeeBlocks(block);
}

void Document::setType(Document::Type type)
{
    resolve()->setType(type);
}

Document::Type Document::getType()
{
    return resolve()->getType();
}

std::string Document::getExtension()
{
    return resolve()->getExtension();
}

pBear::corre::Document::operator bool() const
{
    return ptr.get();
}

void Document::rename(string newName)
{
  resolve()->rename(newName);
}
