#include "project.h"
#include "pbexeption.h"
#include "event.h"

#include <unistd.h>
#include <boost/regex.hpp>

using namespace pBear::corre;



#include <iostream>

RealProject* Project::resolve() const
{
    if (!ptr.get())
    {
        _pbExeption << "resolve()";
        _pbExeption << "pBear::corre::proyect";
        _pbExeption << "no valid proyect";
        throw _pbExeption;
    }
    return ptr.get();
}

Project::Project(std::string name, openDocuments *ops)
{
    ptr = std::make_shared<RealProject>(name, ops);
    ptr->setPTR(ptr);
}

Project::Project(const Project &pro)
{
    ptr = pro.ptr;
}

Project::Project(std::shared_ptr<RealProject> pro)
{
    ptr = pro;
}

void Project::clear()
{
    resolve()->clear();
}

Project::iterator     Project::begin() {
    return resolve()->begin();
}

Project::iterator     Project::end() {
    return resolve()->end();
}

std::string Project::getName() {
    return resolve()->getName();
}


void         Project::setName(string _name)
{
    resolve()->setName(_name);
}

bool         Project::elemine(const Document &doc)
{
    return resolve()->elemine(doc);
}

bool         Project::elemine(iterator it)
{
    return resolve()->elemine(it);
}

Project::iterator    Project::insert(std::string name)
{
    return insert(Document(name));
}

Project::iterator    Project::insert(std::string name, std::string text)
{
    return insert(Document(name, text));
}

Project::iterator    Project::insert(Document doc)
{
    return resolve()->insert(doc);
}

Project::iterator     Project::find(Document &doc)
{
    return resolve()->find(doc);
}

Project::iterator     Project::find(DOC doc)
{
    return resolve()->find(doc);
}

Project::iterator Project::find(std::string name)
{
    return resolve()->find(name);
}

bool operator<(const Document& _doc1,const Document& _doc2) {
    return (&_doc1<&_doc2)? true: false;
}

std::string&          Project::getComand()
{
    return resolve()->getComand();
}

void                  Project::setCompileMode(proyCompile mode)
{
    resolve()->setCompileMode(mode);
}

proyCompile           Project::getCompileMode()
{
    return resolve()->getCompileMode();
}

void                  Project::addHeaderPath(std::string path)
{
    resolve()->addHeaderPath(path);
}

void                  Project::addLibraryPath(std::string path)
{
    resolve()->addLibraryPath(path);
}

void                     Project::addEquivalentID(int id, PROY _pro)
{
    resolve()->addEquivalentID(id, _pro);
}

PROY        Project::getEquivalentID(int id)
{
    return resolve()->getEquivalentID(id);
}

std::map<int, PROY>&     Project::getEquivalentIDs()
{
    return resolve()->getEquivalentIDs();
}

std::string              Project::getLanguage()
{
    return resolve()->getLanguage();
}

void                     Project::setLanguage(std::string language)
{
    resolve()->setLanguage(language);
}

const std::vector<LanguageDatas::Item>&  Project::getItems()
{
    return resolve()->getItems();
}

LanguageDatas::Item               Project::getItem(std::string item)
{
    return resolve()->getItem(item);
}

std::string                       Project::getFileType(std::string name)
{
    return resolve()->getFileType(name);
}

std::string                       Project::getRealToItem(FILEITEMTYPE item)
{
    return resolve()->getRealToItem(item);
}

FILEITEMTYPE                      Project::getItemToReal(std::string item, int id)
{
    return resolve()->getItemToReal(item, id);
}

void                              Project::addItem(std::string name, FILEITEMTYPE item, int id)
{
    resolve()->addItem(name, item, id);
}

std::vector<FILEITEMTYPE>         Project::getRealItemsForId(int id)
{
    return resolve()->getRealItemsForId(id);
}

bool                             Project::hasRealItem(FILEITEMTYPE item)
{
    return resolve()->hasRealItem(item);
}

bool                            Project::hasItem(std::string itemName) const
{
    return resolve()->hasItem(itemName);
}

bool                              Project::hasItemForId(int id)  const
{
    return resolve()->hasItemForId(id);
}

void                            Project::remove()
{
    resolve()->remove();
}

bool                            Project::isEmpty()
{
    return resolve()->isEmpty();
}

int                             Project::size()
{
    return resolve()->size();
}

void                            Project::saveDocs()
{
    resolve()->saveDocs();
}

bool                             Project::containDoc(const Document &doc) const
{
    return resolve()->containDoc(doc);
}

bool                             Project::containDoc(DOC doc)             const
{
    return resolve()->containDoc(doc);
}

std::shared_ptr<RealProject> Project::getPtr()
{
    return ptr;
}

bool                     Project::containEquivalentId(PROY pro)       const
{
    return resolve()->containEquivalentId(pro);
}

bool                      Project::operator<(const Project& pro)           const
{
    return *ptr < *pro.ptr;
}

bool                       Project::operator>(const Project& pro)          const
{
    return *ptr > *pro.ptr;
}

bool                        Project::operator==(const Project& pro)        const
{
    return *ptr == *pro.ptr;
}

bool                         Project::operator!=(const Project& pro)       const
{
    return *ptr != *pro.ptr;
}

void                         Project::removeRealItemsFor(int id)
{
    resolve()->removeRealItemsFor(id);
}

Document Project::newDocument(std::string name)
{
    return *insert(Document(name));
}

Document Project::newDocument(std::string name, std::string &text)
{
    return *insert(Document(name, text));
}
Document Project::newDocument(std::string name, std::string &&text)
{
    return newDocument(name, text);
}

openDocuments *Project::getOpenDocuments()
{
    return resolve()->getOpenDocuments();
}

UpdateBlockInfo *Project::getUpdateBlockInfo()
{
    return resolve()->getUpdateBlockInfo();
}

void Project::setComandStructure(LanguageDatas::ComandStructure comandStructure)
{
    resolve()->setComandStructure(comandStructure);
}

LanguageDatas::ComandStructure *Project::getComandStructure()
{
    return resolve()->getComandStructure();
}


pBear::corre::Project::operator bool() const
{
    return ptr.get();
}

