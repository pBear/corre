#include "realproject.h"

#include "pbexeption.h"
#include "event.h"

#include <unistd.h>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <string>

using namespace pBear::corre;



#include <iostream>


RealProject::RealProject(std::string name, openDocuments *ops):
    name(name), ops(ops)
{
    if (!boost::filesystem::exists(name))
    {
	throw ExceptionNotValidProjectPah(name + " not exists");
    }
  
    items.reserve(10);
    adtitionalITem = {{""}, {""}, "", "", "/home/aron/pBear/corre/icons/Additional.png", "", true, false, true, true};
    items.push_back(adtitionalITem);
}

void RealProject::setPTR(std::shared_ptr<RealProject> ptr)
{
    this->ptr = ptr;
}

RealProject::~RealProject()
{
    clear();
}

void RealProject::clear()
{
    docs.clear();
}

RealProject::iterator     RealProject::begin() {
    return docs.begin();
}

RealProject::iterator     RealProject::end() {
    return docs.end();
}

std::string RealProject::getName() {
    return name;
}

void         RealProject::setName(string _name)
{
    if (!boost::filesystem::exists(_name))
    {
	throw ExceptionNotValidProjectPah(_name + " not exists");
    }
    name = _name;
    ops->getEvent()->executeFuncProUpdate(Project(ptr.lock()));
}

bool         RealProject::elemine(const Document &doc)
{
    if (!docs.erase(doc))
    {
        return true;
    }
    return false;
}

bool         RealProject::elemine(iterator it)
{
    Document tmp = *it;
    if (docs.erase(it) == docs.end())
    {
        return true;
    }
    return false;
}

RealProject::iterator    RealProject::insert(std::string name)
{
    return insert(Document(name));
}

RealProject::iterator    RealProject::insert(string name, string text)
{
    return insert(Document(name,text));
}

RealProject::iterator    RealProject::insert(Document doc)
{
    iterator it_ret;
    if (find(doc) != end()) return find(doc);
    if ((it_ret = find(doc.getName())) != end()) return it_ret;
    
    auto return_data = docs.insert(doc).first;
    doc.setProyect(ptr.lock());
    if (doc.getItem().empty())
    {
        doc.updateLanguage();
    }

    auto it = std::find_if(items.begin(), items.end(), [doc](pBear::corre::LanguageDatas::Item item)
    {
        return doc.getItem() == item.name;
    });

    if (it == items.end())
    {
        auto fun = [doc](std::vector<LanguageDatas::Item> &items)
        {
            return  std::find_if(items.begin(), items.end(), [doc](LanguageDatas::Item item)
            {
                return item.name == doc.getItem();
            });
        };
        for (auto x: *Language::instance())
        {
            LanguageDatas *lanData = x.second;
            auto items = lanData->getItems();
            auto it = fun(items);
            if (it != items.end())
            {
                this->items.push_back(*it);
                ops->getEvent()->executeFuncLanguageChanged(ptr.lock());
            }
        }
    }

    if (ops && ops->getEvent())
    {
        ops->getEvent()->executeFuncNewDoc(doc);
    }
    return return_data;
}

RealProject::iterator     RealProject::find(const Document &doc)
{
    return docs.find(doc);
}

RealProject::iterator     RealProject::find(DOC doc)
{
    for (RealProject::iterator it = begin(); it != end(); it++)
    {
        if (it->constainEquivalentId(doc))
        {
            return it;
        }
    }
    return end();
}

RealProject::iterator RealProject::find(std::string name)
{
    for (auto it = docs.begin(); it != docs.end(); it++)
    {
        if (it->getName() == name)
        {
            return it;
        }
    }
    return end();
}

/*bool operator<(const document& _doc1,const document& _doc2) {
    return (&_doc1<&_doc2)? true: false;
}*/

std::string&          RealProject::getComand()
{
    return comand;
}

void                  RealProject::setCompileMode(proyCompile mode)
{
    this->mode = mode;
}

proyCompile           RealProject::getCompileMode()
{
    return mode;
}

void                  RealProject::addHeaderPath(std::string path)
{
    headerPathes[path] = false;
}

void                  RealProject::addLibraryPath(std::string path)
{
    libraryPathes[path] = false;
}

void                     RealProject::addEquivalentID(int id, PROY _pro)
{
    if (!_pro)
    {
        _pbExeption << "addEquivalentID";
        _pbExeption << "RealProyect";
        _pbExeption << "the parameter PROY _pro = " << _pro;
        throw _pbExeption;
    }
    equivalentIDs[id] = _pro;
}

PROY        RealProject::getEquivalentID(int id)
{
    return equivalentIDs[id];
}

std::map<int, PROY>&     RealProject::getEquivalentIDs()
{
    return equivalentIDs;
}

std::string              RealProject::getLanguage()
{
    return language;
}

void                     RealProject:: setLanguage(std::string language)
{
    _pbExeption.addFunction("pBear::RealProyect::setLanguage(std::string language)");

    if (this->language == language) return;

    this->language = language;

    items.clear();
    itemToReal.clear();
    ubi.reset();

    if (!language.empty())
    {
        items    = Language::instance()->getLanguage(language)->getItems();
        ubi      = Language::instance()->getLanguage(language)->getUpdateBlockInfo();
    }

    items.push_back(adtitionalITem);

    for (Document x: *this)
    {
        x.updateLanguage();
    }

    if (ops && ops->getEvent())
    {
        ops->getEvent()->executeFuncLanguageChanged(ptr.lock());
    }
}

const std::vector<LanguageDatas::Item>&  RealProject::getItems()
{
    return items;
}

LanguageDatas::Item               RealProject::getItem(std::string item)
{
    for (LanguageDatas::Item x: items)
    {
        if (x.name == item)
        {
            return x;
        }
    }
    return  LanguageDatas::Item();
}

std::string                       RealProject::getFileType(std::string name)
{
    auto fun = [this, name](std::vector<LanguageDatas::Item> items)
    {
        return  std::find_if(items.begin(), items.end(), [this, name](LanguageDatas::Item item)
        {
            for (std::string ex: item.extencions)
            {
                if (std::regex_search(name.begin(), name.end(), std::regex(ex + "$")))
                {
                    return true;
                }
            }
            return false;
        });
    };
    auto it = fun(items);
    if (it != items.end())
    {
        return it->name;
    }
    for (auto x: *Language::instance())
    {
        LanguageDatas *lanData = x.second;
        auto items = lanData->getItems();
        auto it = fun(items);
        if (it != items.end())
        {
            return it->name;
        }
    }
    return "";
}

std::string                       RealProject::getRealToItem(FILEITEMTYPE item)
{
    for (std::pair<std::string, std::map<int, FILEITEMTYPE>> x: itemToReal)
    {
       for (std::pair<int, FILEITEMTYPE> data: x.second)
       {
           if (data.second == item)
           {
               return  x.first;
           }
       }
    }
    _pbExeption << "getRealToItem(FILEITEMTYPE item)";
    _pbExeption << "pBear::RealProyect";
    _pbExeption << "theear are not equivalent item in this RealProyect";
    throw _pbExeption;
}

FILEITEMTYPE                      RealProject::getItemToReal(std::string item, int id)
{
    return itemToReal[item][id];
}

void                              RealProject::addItem(std::string name, FILEITEMTYPE item, int id)
{
    itemToReal[name][id] = item;
}

std::vector<FILEITEMTYPE>         RealProject::getRealItemsForId(int id)
{
    std::vector<FILEITEMTYPE> datas;
    for (auto it = itemToReal.begin(); it != itemToReal.end(); it++)
    {
        auto x = it->second.find(id);
        if (x != it->second.end())
        {
            datas.push_back(x->second);
        }
    }
    return datas;
}

bool                             RealProject::hasRealItem(FILEITEMTYPE item)
{
    for (std::pair<std::string, std::map<int, FILEITEMTYPE>> x: itemToReal)
    {
       for (std::pair<int, FILEITEMTYPE> data: x.second)
       {
           if (data.second == item)
           {
               return  true;
           }
       }
    }
    return false;
}

bool                            RealProject::hasItem(std::string itemName) const
{
    for (LanguageDatas::Item x: items)
    {
        if (x.name == itemName)
        {
            return true;
        }
    }
    return false;
}

bool                              RealProject::hasItemForId(int id)  const
{
    for (auto it = itemToReal.begin(); it != itemToReal.end(); it++)
    {
        if (it->second.find(id) != it->second.end())
        {
            return true;
        }
    }
    return false;
}

void                            RealProject::remove()
{

}

bool                            RealProject::isEmpty()
{
    return docs.empty();
}

int                             RealProject::size()
{
    return docs.size();
}

void                            RealProject::saveDocs()
{
    for (Document doc: docs)
    {
        doc.save();
    }
}

bool                             RealProject::containDoc(const Document &doc) const
{
    return docs.find(doc) != docs.end();
}

bool                             RealProject::containDoc(DOC doc)             const
{
    for (Document x: docs)
    {
        if (x.constainEquivalentId(doc))
        {
            return true;
        }
    }
    return false;
}

bool                     RealProject::containEquivalentId(PROY pro)       const
{
    for (std::pair<int, PROY> x: equivalentIDs)
    {
        if (x.second == pro)
        {
            return true;
        }
    }
    return false;
}

bool                      RealProject::operator<(const RealProject &pro)           const
{
    return this < &pro;
}

bool                       RealProject::operator>(const RealProject& pro)          const
{
    return this > &pro;
}

bool                        RealProject::operator==(const RealProject& pro)        const
{
    return this == &pro;
}

bool                         RealProject::operator!=(const RealProject& pro)       const
{
    return this != &pro;
}

void                         RealProject::removeRealItemsFor(int id)
{
    for (auto x = itemToReal.begin(); x != itemToReal.end(); x++)
    {
        std::map<int, FILEITEMTYPE>::iterator it;
        it = x->second.find(id);
        if (it != x->second.end())
        {
            x->second.erase(it);
        }
    }
}

openDocuments *RealProject::getOpenDocuments()
{
    return ops;
}

UpdateBlockInfo *RealProject::getUpdateBlockInfo()
{
    return ubi.get();
}

void RealProject::setComandStructure(LanguageDatas::ComandStructure comandStructure)
{
    this->comandStructure = comandStructure;
    if (this->comandStructure.path.empty() and this->comandStructure.executable.empty())
    {
      this->comandStructure.path = name;
      this->comandStructure.executable = "./a.out";
    }
}

LanguageDatas::ComandStructure *RealProject::getComandStructure()
{
    return &comandStructure;
}


