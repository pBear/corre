#include "help.h"

#include <boost/regex.hpp>
#include <iostream>
#include <fstream>

std::array<char*,20> splitStringToArgs(std::string str)
{
    boost::smatch            results;
    boost::regex             exp("(['].*[']|(\\w|\\d|\\+|-|_|/|\\.|,)+)");
    std::array<char*, 20>    args;

    boost::regex_search(str, results, exp);

    boost::sregex_iterator begin(str.begin(), str.end(), exp);
    boost::sregex_iterator end =boost::sregex_iterator();


    int i = 0;
    for (boost::sregex_iterator it = begin; it != end; it++)
    {
        args[i] = new char[it->str().size() + 1];
        for (int j = 0, n = it->str().size(); j < n; j++)
        {
            args[i][j] = it->str()[j];
        }
        args[i][it->str().size()] = '\0';
        i++;
    }


    args[i] = 0;


    return args;
}

std::array<char, 20> unifySendData(std::string &name, std::vector<char> &opt , std::string data)
{
    std::array<char, 20> tmp;
    int                  pos = 0;
    tmp.fill('\0');
    for (int i = 0, n = name.size(); i < n; i++)
    {
        tmp[i] = name[i];
    }
    pos = name.size();
    tmp[pos] = '#';
    pos++;
    for (int  i = 0, n = opt.size(); i < n; pos++, i++)
    {
        tmp[pos] = opt[i];
    }
    tmp[pos] = '@';
    pos++;
    for (int i = 0, n = data.size(); i < n; pos++, i++)
    {
        tmp[pos] = data[i];
    }
    return tmp;
}

std::vector<std::array<char, 20> > splitStringToSendArrays(std::string name, std::vector<char> opt ,std::string data)
{
    std::vector<std::array<char, 20>> dataReturn;
    int                               baseSize   = name.size() + opt.size() + 3;
    int                               usefulSize = 19 - baseSize;
    int                               pos        = 0;

    opt.insert(opt.begin(), 'E');

    if (!data.empty())
    {
        for (int i = 0, n = data.size(); i < n; i++)
        {
            if (i % usefulSize == 0 and i > 0)
            {
                if (baseSize + data.size() <= 19)
                {
                    opt[0] = 'A';
                }
                else if (i == 19 - baseSize)
                {
                    opt[0] = 'B';
                }
                else
                {
                    opt[0] = 'C';
                }
                dataReturn.push_back(unifySendData(name, opt, data.substr(pos, i - pos)));
                pos = i;
            }
        }
        if ((data.size() - pos) > 0)
        {
            opt[0] = 'E';
            dataReturn.push_back(unifySendData(name, opt, data.substr(pos, data.size() - pos)));
        }
    }
    else
    {
        dataReturn.push_back(unifySendData(name, opt, ""));
    }
    return dataReturn;
}



#include <sstream>
#include <iomanip>

std::string extractPart(std::string text,
                        std::string before,
                        std::string after,
                        unsigned    *offset)
{
    if (offset != NULL)
        text.erase(text.begin(), text.begin() + *offset);

    boost::regex ex('(' + before  + ')' + "(.*)" + '(' + after + ')');
    boost::smatch m;
    boost::regex_search(text, m, ex);

    if (m.empty())
        *offset = -1;
    else if (offset != NULL)
        *offset = m.position(2) + m.length(2);

    return m[2].str();
}

std::string extractPart(std::string text, std::string expres)
{
    boost::regex ex(expres);
    boost::smatch m;
        boost::regex_search(text, m, ex);
    return m[0].str();
}

std::vector<std::string> extractLines(std::string text)
{
    std::vector<std::string> lines;

    boost::sregex_iterator pos(text.begin(),
                    text.end(),
                    boost::regex("[^\n]+"));
    boost::sregex_iterator end;

    for (; pos != end; pos++)
        lines.push_back(pos->str());

    return lines;
}

int convertToInt(std::string number)
{
    if (number.empty())
        return -1;
    int number_int;
    std::stringstream tran;
    tran << number;
    tran >> number_int;
    return number_int;
}

std::string toStd(int number)
{
    std::stringstream tran;
    tran << number;
    return tran.str();
}

std::string setCorrectSizeType(std::string num)
{
        std::ostringstream st;
        unsigned i = std::stoul(num);
        if (i < 1000)
        {
            st << std::setprecision(3) << i << "kB";
            return st.str();
        }
        if ( i < 1000000)
        {
            st << std::setprecision(3) << (double)i/1000 << "MB";
            return st.str();
        }
        if (i < 1000000000)
        {
            st << std::setprecision(3) << (double)i/1000000 << "GB";
            return st.str();
        }
        st << std::setprecision(3) << (double)i/1000000000 << "TB";
        return st.str();
}

std::string extractPath(std::string fullName)
{
    boost::regex  ex("[/][^/]*$");
    return boost::regex_replace(fullName, ex, "");
}

std::string extractName(std::string fullName)
{
    boost::regex  ex("[/]([^/]*)$");
    boost::smatch m;

    boost::regex_search(fullName, m, ex);

    return m.str(1);
}

std::string regexLiteral(std::string name)
{
    std::string tmp;
    for (char c: name)
    {
        if (c == '.' or c == '(' or c == ')')
        {
            tmp += std::string("\\") + std::string(1, c);
        }
        else
        {
            tmp += std::string(1, c);
        }
    }
    return tmp;
}

std::string getSpecificFileEntryByLine(std::string path, std::string lineName, unsigned entry)
{
    std::string   tmp;
    std::ifstream in(path);
    while (std::getline(in, tmp))
    {
        boost::sregex_iterator it(tmp.begin(), tmp.end(), boost::regex("\\w"));
        boost::sregex_iterator end;
        if ( it != end and it->str() == lineName)
        {
            for (int i = 0; i < entry; i++)
            {
                it++;
            }
            return it->str();
        }
    }
    std::runtime_error("Nota found");
}

std::string getSpecificFileEntry(std::string path, unsigned entry)
{
    std::string   tmp;
    std::ifstream in(path);
    for (unsigned i = 0; i < entry; i++)
    {
        in >> tmp;
    }
    return tmp;
}
