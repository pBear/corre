#include "block.h"
#include "document.h"

#include "event.h"
#include "pbexeption.h"

using namespace pBear::corre;


int Block::getDistance() const
{
    if (collapsed)
    {
        return 0;
    }
    else
    {
        return endBlock->line - line + diferenceDistance;
    }
}

void Block::setDiferenceDistance(int value)
{
    diferenceDistance += value;
}

bool Block::isCollapaseble() const
{
    return endBlock != nullptr;
}

void Block::updateLanguage()
{
    std::string language = getDocument().getProyect().getLanguage();
    if (!language.empty())
    {
        data = Language::instance()->getLanguage(language)->getBlockData(this);
    }
}

Block::BlockData *Block::getData()
{
    return data.get();
}

std::list<DiagnosticData>& Block::getDiagnostics()
{
    return diags;
}

void Block::setDiagnostics(std::list<DiagnosticData> diags)
{
    if (diags != this->diags)
    {
        this->diags = diags;
    }
}

Document Block::getDocument()
{
    return Document(doc.lock());
}



Block::Block(int begin, int end, int line, std::weak_ptr<RealDocument> doc):
    doc(doc)
{
    update(begin, end, line);
}

void Block::update(int begin, int end, int line)
{
    this->begin = begin;
    this->end   = end;
    this->line  = line;

    if (!data)
    {
        updateLanguage();
    }
}

int Block::getBegin() const
{
    return begin;
}

int Block::getEnd() const
{
    return end;
}

int Block::getLine() const
{
    return line;
}

void* Block::getEquivalentId() const
{
    return equivalentId;
}

void Block::setEquivalentId(void *equivalentId)
{
    this->equivalentId = equivalentId;
}

bool Block::getCollapsed() const
{
    return collapsed;
}

Block *Block::getEndBlock() const
{
    return endBlock;
}

void Block::setEndBlock(Block *block)
{
    endBlock = block;
}

void Block::setCollapsed(bool state)
{
    if (!isCollapaseble() and state)
    {
        _pbExeption << "eraseBlocks(iterator begin, int pos, int size)";
        _pbExeption << "pBear::corre::Block";
        _pbExeption << "it is not collapsable ";
        throw _pbExeption;
    }
    collapsed = state;
    getDocument().hideSeeBlocks(*this);
}
