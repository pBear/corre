﻿#include "event.h"
#include "pbexeption.h"
#include "help.h"

#include <opendocuments.h>
#include <iostream>
#include <boost/regex.hpp>
#include <dirent.h>
#include <boost/filesystem.hpp>

pBear::corre::openDocuments     *_openDocuments = NULL;

using namespace pBear::corre;

//.........dirEnt..............

dirEnt::dirEnt(std::string _name, std::string _completName, type _ty)
{
    name        = _name;
    completName = _completName;
    ty          = _ty;
}

dirEnt::~dirEnt()
{
}

dirEnt::type dirEnt::getType()
{
    return ty;
}

std::string dirEnt::getName()
{
    return name;
}

std::string dirEnt::getCompletName()
{
    return completName;
}

std::string dirEnt::getPrefix()
{
    boost::regex  ex("^/([^/]*/)*");
    boost::smatch m;
    if (!boost::regex_search(completName, m, ex))
    {
        return "";
    }

    return m.str().substr(0, m.str().size() - 1);
}

void dirEnt::setName(std::string _name)
{
    name = _name;
}

void dirEnt::setType(type _ty)
{
    ty = _ty;
}

void dirEnt::setCompletName(std::string _name)
{
    completName = _name;
}

//.........openDocuments.........

openDocuments::~openDocuments()
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    for (auto x: pro)
    {
        closeProyect(x);
    }
}

openDocuments::iterator openDocuments::begin()
{
    return pro.begin();
}

openDocuments::iterator openDocuments::end()
{
    return pro.end();
}

bool     openDocuments::existProyect(PROY name)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::existProyect(PROY name)");
    for (Project x: pro)
    {
        if (x.containEquivalentId(name))
        {
            return true;
        }
    }
    return false;
}

bool   openDocuments::existProyect(Project &pr)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::existProyect(proyect &pr)");
    return pro.find(pr) != pro.end();
}

bool     openDocuments::existDocument(DOC doc)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::existDocument(DOC doc)");
    for (Project x: pro)
    {
        if (x.containDoc(doc))
        {
            return true;
        }
    }
    return false;
}

bool    openDocuments::existDocument(const Document &doc)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::existDocument(const document &doc)");
    for (Project x: pro)
    {
        if (x.containDoc(doc))
        {
            return true;
        }
    }
    return false;
}

bool    openDocuments::existItem(FILEITEMTYPE item)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::existItem(FILEITEMTYPE item)");
    for (Project x: pro)
    {
        if (x.hasRealItem(item))
        {
            return true;
        }
    }
    return false;
}

Document openDocuments::traductDoc(DOC _doc)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::traductDoc(DOC _doc)");

    Project::iterator it;
    for (Project x: pro)
    {
        if ( (it = x.find(_doc)) != x.end())
        {
            return *it;
        }
    }
    _pbExeption << "traductDoc(DOC _doc)";
    _pbExeption << "pBear::corre::openDocuments";
    _pbExeption << "the parameter _doc is " << _doc;
    throw _pbExeption;
}

Project  openDocuments::traductPro(PROY _pro)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::traductPro(PROY _pro)");

    for (Project x: pro)
    {
        if (x.containEquivalentId(_pro))
        {
            return x;
        }
    }
    _pbExeption << "traductPro(PROY _pro)";
    _pbExeption << "pBear::corre::openDocuments";
    _pbExeption << "the document no exist ";
    throw _pbExeption;
}

void      openDocuments::closeProyect(Project _pro)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::closeProyect(proyect _pro)");
    if (!existProyect(_pro))
    {
        _pbExeption << "closeProyect(PROY _pro)";
        _pbExeption << "pBear::corre::openDocuments";
        _pbExeption << "the proyect no exist:" ;
        throw _pbExeption;
    }

    pro.erase(_pro);
}

void openDocuments::setCurrentDoc(Document &_doc)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::setCurrentDoc(document _doc)");
    if (!_doc) return;
    if(!existDocument(_doc))
    {
        _pbExeption << "openDocuments::corre::setCurrentDoc(document _doc)";
        _pbExeption << "pBear::openDocuments";
        _pbExeption << "the document not exist" ;
        throw _pbExeption;
    }

    if (Document(cDoc.lock()) != _doc)
    {
        cDoc = _doc.getPtr();
        setCurrentPro(_doc.getProyect());
        event->executeFuncChangeCurDoc(_doc);
    }
}

Document openDocuments::getCurrentDoc() const
{
    _pbExeption.addFunction("pBear::corre::openDocuments::getCurrentDoc() const");
    return Document(cDoc.lock());
}


Project  openDocuments::getCurrentPro() const
{
    _pbExeption.addFunction("pBear::corre::openDocuments::getCurrentPro() const");
    return Project(cPro.lock());
}

void      openDocuments::setCurrentPro(Project& pro)
{
    _pbExeption.addFunction("pBear::corre::openDocuments::setCurrentPro(proyect pro)");

    if (pro != Project(cPro.lock()))
    {
        cPro = pro.getPtr();
        event->executeFuncChangeCurProy(pro);
    }
}

void  openDocuments::addProyect(Project dat)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::addProyect(proyect &dat)");

    if (pro.find(dat) ==  pro.end())
    {
        pro.insert(dat);
        event->executeFuncNewProyect(dat);
        //LanguageDatas::Item item = tmp.getItem(getDefaultConfigItem());
    }
    else
    {
        _pbExeption << "addProyect";
        _pbExeption << "openDocuments";
        _pbExeption << "the proyect is alrready added";
        throw _pbExeption;
    }
}

bool     openDocuments::existFile(string name)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::existFile(string name)");
    return boost::filesystem::exists(name);
}

void openDocuments::removePro(Project _pro)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::removePro(proyect _pro)");
    if (!existProyect(_pro))
    {
        _pbExeption << "removePro";
        _pbExeption << "openDocuments";
        _pbExeption << "the proyect no exist";
        throw _pbExeption;
    }

    //if exist a proyect directory remove it
    _pro.remove();

    //close the proyect
    closeProyect(_pro);
}

bool openDocuments::docEmpty()
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::docEmpty()");
    for (Project x: pro)
    {
        if (!x.isEmpty())
        {
            return false;
        }
    }
    return true;
}

itemType  openDocuments::type(ITEM _item)
{
    std::lock_guard<std::recursive_mutex > locker(mu1);
    _pbExeption.addFunction("pBear::openDocuments::type(ITEM _item)");
    if (existProyect(_item))
    {
        return itemType::PROYECT;
    }
    if (existDocument(_item))
    {
        return itemType::DOCUMENT;
    }
    if (existItem(_item))
    {
        return itemType::GROUP;
    }
    return itemType::NONE;
}

std::vector<std::string> openDocuments::redRDir(std::string name)
{
     std::lock_guard<std::recursive_mutex> locker(mu1);
     _pbExeption.addFunction("openDocuments::corre::redRDir(string name)");
     vector<string>  tmp;

     if (!boost::filesystem::exists(name) or !boost::filesystem::is_directory(name))
     {
         _pbExeption << "pBear::openDocuments::redRDir(std::string name)";
         _pbExeption << "pBear::openDocuments";
         _pbExeption << "ist not a directory or not exist";
         throw _pbExeption;
     }

     boost::filesystem::recursive_directory_iterator it(name);
     boost::filesystem::recursive_directory_iterator end;

     for (; it != end; it++)
     {
         if (it->path().filename() == ".." or it->path().filename() == ".")
         {
             continue;
         }
         tmp.push_back(it->path().string());
     }

     return tmp;
}

bool  openDocuments::proEmpty()
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::proEmpty()");
    return pro.size() == 0;
}

std::vector<Document> openDocuments::getUnsavedDocs(Document doc)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::openDocuments::getUnsavedDocs(pBear::document doc)");
    vector<Document> tmp_vec;


    Project          tmp_pro =  doc.getProyect();
    for (auto i = tmp_pro.begin(); i != tmp_pro.end(); ++i)
    {
        if (!(*i).getSaved())
        {
            tmp_vec.push_back(*i);
        }
    }
    return tmp_vec;
}

const std::vector<Document> openDocuments::getDocData()
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::openDocuments::getDocData()");
    std::vector<Document> docs;

    for (Project x: pro)
    {
        for (Document y: x)
        {
            docs.push_back(y);
        }
    }
    return docs;
}

Document     openDocuments::getDocByName(std::string path)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("openDocuments::corre::getDocByName(std::string path)");
    Project::iterator it;
    for (Project x: pro)
    {
        if ((it = x.find(path)) != x.end())
        {
            return *it;
        }
    }
    return Document();
}

bool         openDocuments::existDocByName(std::string path)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("pBear::corre::openDocuments::existDocByName(std::string path)");
    Project::iterator it;
    for (Project x: pro)
    {
        if ((it = x.find(path)) != x.end())
        {
            return true;
        }
    }
    return false;
}

std::vector<dirEnt> openDocuments::getFolderEntries(std::string path, bool getUnique)
{
    _pbExeption.addFunction("pBear::corre::openDocuments::getFolderEntries(std::string path, bool getUnique)");
    std::vector<dirEnt> entrs;
    dirEnt              tmp;
    DIR                 *d     = NULL;
    struct dirent       *dir   =NULL;
    bool                found = false;

    boost::regex ex_cpp(".*\\.(cpp|c|cxx)$");
    boost::regex ex_hpp(".*\\.(hpp|h|hxx)$");
    boost::regex ex_make("CMakeLists.txt");

    d = opendir(path.c_str());
    if (d)
    {
      while ((dir = readdir(d)) != NULL and (!getUnique or !found))
      {
        if (std::strlen(dir->d_name) == 1 and dir->d_name[0] == '.')
        {
            continue;
        }
        tmp.setName(dir->d_name);
        if (std::string(dir->d_name) != "/")
        {
            tmp.setCompletName(path + "/" + dir->d_name);
        }
        else
        {
            tmp.setCompletName(path + dir->d_name);
        }
        if (dir->d_type == DT_DIR)
        {
            tmp.setType(dirEnt::FOLDER);
        }
        else
        {
            tmp.setType(dirEnt::DOCUMENT);
        }
        if (std::string(dir->d_name)[0] != '.')
        {
            found = true;
        }

        entrs.push_back(tmp);
      }
      //closedir(d);
    }
    closedir(d);
    std::sort(entrs.begin(), entrs.end(), [](dirEnt ent1, dirEnt ent2)
    {
        if (ent1.getType() == dirEnt::FOLDER and ent2.getType() != dirEnt::FOLDER)
        {
            return true;
        }
        if (ent2.getType() == dirEnt::FOLDER and ent1.getType() != dirEnt::FOLDER)
        {
            return false;
        }
        return ent1.getName() < ent2.getName();
    });
    return entrs;
}

/*bool openDocuments::compareSearch(std::string file, std::string search)
{
    return boost::regex_search(file, boost::regex(regexLiteral(search)));
}*/

Project                 openDocuments::getProByGroup(FILEITEMTYPE _group)
{
    std::lock_guard<std::recursive_mutex> locker(mu1);
    _pbExeption.addFunction("openDocuments::corre::getProByGroup(FILEITEMTYPE _group)");
    for (Project x: pro)
    {
        if (x.hasRealItem(_group))
        {
            return x;
        }
    }
    _pbExeption << "getProByGroup(FILEITEMTYPE _group)";
    _pbExeption << "openDocuments";
    _pbExeption << "the group does not exist";
    throw _pbExeption;
}

Project openDocuments::newProject(std::string name)
{
    Project tmp(name, this);
    addProyect(tmp);

    Document doc = tmp.newDocument("/home/aron/default.jsonConf");
    doc.read();

    return tmp;
}

Project openDocuments::newProject(std::string name, std::string language, std::string mode)
{
    Project tmp(name, this);
    addProyect(tmp);;

    tmp.setLanguage(language);
    auto data = pBear::corre::Language::instance()->getLanguage(language)->getComandStructure();
    auto it = std::find_if(data.begin(), data.end(), [mode](pBear::corre::LanguageDatas::ComandStructure comandStructure)
    {
           return comandStructure.name == mode;
    });
    tmp.setComandStructure(*it);
    
    if (!boost::filesystem::exists(tmp.getName() + "/default.jsonConf"))
    {
      std::ofstream os(tmp.getName() + "/default.jsonConf");
      os << "{" << std::endl
         << "   \"configurations\":\"\"," << std::endl
	 << "   \"header files\":   ["    << std::endl
	 << ""   << std::endl
	 << "    ]," << std::endl
	 << "   \"source files\":   [" << std::endl
	 << "   ]" << std::endl
	 << "}";
      os.close();
      
    }
    
    Document doc = tmp.newDocument(tmp.getName() + "/default.jsonConf");
    doc.read();

    return tmp;
}

int openDocuments::size() const
{
    return pro.size();
}






/*bool                     openDocuments::renameDoc(document *oldDoc, document &newDoc)
{
    _pbExeption.addFunction("pBear::openDocuments::renameDoc(document *oldDoc, document &newDoc)");

    if (boost::filesystem::exists(newDoc.getName()) or oldDoc->getName() == newDoc.getName())
    {
        return false;
    }

    boost::filesystem::rename(oldDoc->getName(), newDoc.getName());
    return true;
}*/

/*std::string              openDocuments::extractLocationFromPath(std::string path)
{
    _pbExeption.addFunction("pBear::openDocuments::extractLocationFromPath(std::string path)");
    boost::smatch m;
    boost::regex_match(path,m, boost::regex("^(" + path + ")/[^/]*"));
    return m.str(1);
}*/

void openDocuments::setDefaultConfigItem(string item)
{
  defaultConfigItem = item;
}

string openDocuments::getDefaultConfigItem()
{
  return defaultConfigItem;
}

void openDocuments::setEvent(pBear::corre::Event *event)
{
    this->event = event;
}


Event *openDocuments::getEvent()
{
    return event;
}
